<?php
session_start();
require_once 'class.user.php';
$user = new USER();

if($user->is_logged_in()!="")
{
	$user->redirect('home.php');
}

if(isset($_POST['btn-submit']))
{
	$email = $_POST['txtemail'];
	
	$stmt = $user->runQuery("SELECT userID FROM tbl_users WHERE userEmail=:email LIMIT 1");
	$stmt->execute(array(":email"=>$email));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);	
	if($stmt->rowCount() == 1)
	{
		$id = base64_encode($row['userID']);
		$code = md5(uniqid(rand()));
		
		$stmt = $user->runQuery("UPDATE tbl_users SET tokenCode=:token WHERE userEmail=:email");
		$stmt->execute(array(":token"=>$code,"email"=>$email));
		
		$message= "
				   Hello , $email
				   <br /><br />
				   We received a request to reset your password,please click the link below to reset your password.If you did not send this request,Ignore this email.
				   <br /><br />
				   Click this Link To Reset Your Password 
				   <br /><br />
				   
                   <a href='http://localhost/ulna/resetpass.php?id=$id&code=$code'>click here to reset your password</a>
				   <br /><br />
				   thank you :)
				   ";
		$subject = "Password Reset";
		
		$user->send_mail($email,$message,$subject);
		
		$msg = "<div class='alert alert-success'>
					<button class='close' data-dismiss='alert'>&times;</button>
					We've sent an email to $email.
                    Please click on the password reset link in the email to generate your new password. 
			  	</div>";
	}
	else
	{
		$msg = "<div class='alert alert-danger'>
					<button class='close' data-dismiss='alert'>&times;</button>
					<strong>Sorry!</strong>  this email not found. 
			    </div>";
	}
}
?>
<!DOCTYPE html>
<!--<a href='http://ulna.international/resetpass.php?id=$id&code=$code'>click here to reset your password</a>-->
<html>
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     
          <link rel="apple-touch-icon" sizes="57x57" href="dist/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="dist/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="dist/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="dist/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="dist/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="dist/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="dist/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="dist/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="dist/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="dist/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="dist/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="dist/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="dist/favicon/favicon-16x16.png">
<link rel="manifest" href="dist/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="dist/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/square/red.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
          <link rel="stylesheet" href="dist/css/write.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
     
     
      <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url(http://weloveiconfonts.com/api/?family=iconicfill);
@import url(http://fonts.googleapis.com/css?family=Arvo);
/* iconicfill */
[class*="iconicfill-"]:before {
  font-family: 'IconicFill', sans-serif;
}

body,html{width:100%;height:100%;}
body{font-family:'Arvo'; background: #4f3b5b;background: -moz-radial-gradient(center, ellipse cover,  #4f3b5b 0%, #231733 100%);background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#4f3b5b), color-stop(100%,#231733));background: -webkit-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: -o-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: -ms-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: radial-gradient(ellipse at center,  #4f3b5b 0%,#231733 100%);}

.wrap{width:300px;}
.center{
 top:50%;
 transform:translateY(-50%);
 position:relative;
 margin:auto;}

input{
  display:block;
    width: 300px;
    padding: 15px 0 15px 12px;
    font-family: "Arvo";
    font-weight: 400;
    color: #377D6A;
    background: rgba(0,0,0,0.3);
    border: none;
    outline: none;
    color: #fff;
    text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
    border: 1px solid rgba(0,0,0,0.3);
    border-radius: 4px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px  1px rgba(255,255,255,0.2);
    text-indent: 60px;
    transition: all .3s ease-in-out;
    position: relative;
    font-size: 13px;
}
input:focus{
    text-indent: 12px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.4), 0 1px 1px rgba(255,255,255,0.2);
}

label{
    display: block;
    position: static;
    margin: 0;
    padding: 0;
    color:#fff;
    font-family: 'Arvo';
    font-size: 16px;
}

.wrap-label{
  width: 300px;
  height: 34px;
  position: relative;
  padding: 0;
  margin: 0;
  bottom: -15px;
  overflow: hidden;}

.iconicfill-pen-alt2{
    position: absolute;
    left: 10px;
    color: #fff;
    font-size:19px;
    opacity: 1;
    top: 0;
    transform:translateX(-100px);
}

/* ==== Pen animation ==== */
.move-pen{animation: move-pen 1s ease-in infinite alternate;}
@keyframes move-pen{
    from{transform:translateX(-4px) rotate(6deg);}
    to{transform:translateX(4px) rotate(-3deg);}
}
     </style>    
</head>
 <body oncontextmenu="return false;"> 
       
      
    
     
  <div class="login-logo">
      <a href="index.php"><img src="dist/img/logo3B.png" height="150px" width ="150px"/><b>ULNA</b></a>
  </div>
  <!-- /.login-logo -->
  
   
<form method="post" id="login-form">
       <?php
			if(isset($msg))
			{
				echo $msg;
			}
			else
			{
				?>
              	<div class='alert alert-info'>
				Please enter your email address.You will receive instructions on how to reset your password.!
				</div>  
                <?php
			}
			?>
        
    <br>
     
    <div class="wrap center"> <!-- Just to center ver and hor -->
  <div class="wrap-label">
     <label for="name">Your Email Address</label>
     <p class="iconicfill-pen-alt2"></p>
      
  </div>
  <input type="email" name="txtemail" required />
        
</div>
    
     <div class="wrap center"> <!-- Just to center ver and hor -->
  
    <button  class="btn btn-danger btn-block btn-flat" type="submit" name="btn-submit" >Generate</button>
        
</div>
     <div class="wrap center"> <!-- Just to center ver and hor -->
  <a href="index.php" class="text-center">Return to Login</a>
       
</div>
    </form>
      

      
      
       

    <!--<div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>-->
    <!-- /.social-auth-links -->


           
   
  <!-- /.login-box-body -->

     
     

       <script src="dist/js/write.js"></script> 
    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
     <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
  </body>
</html>