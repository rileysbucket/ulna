<?php
session_start();
//$con=mysqli_connect("localhost:3306","ulna","Ul2017@$#","ulna_db");
$con=mysqli_connect("localhost","root","Genious2016","ulna_db");
if (!isset($_SESSION['lodge'])) {
    header("Location:lodge.php");
}
$res = mysqli_query($con, "SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
$row = mysqli_fetch_array($res);
?>

<?php

	require_once '../con.php';
	if(isset($_GET['delete_id']))
    {
		$stmt_select = $DB_con->prepare('SELECT logos FROM lodges WHERE lodge_id=:upic ');
		$stmt_select->execute(array(':upic'=>$_GET['delete_id']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("logos/".$imgRow['logos']);
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM lodges WHERE lodge_id=:upic');
		$stmt_delete->bindParam(':upic',$_GET['delete_id']);
		$stmt_delete->execute();
		
		header("Location:lodgeprofile.php");
	}

?>


<?php

	require_once '../con.php';
	if(isset($_GET['delete_imageA']))
    {
		$stmt_select = $DB_con->prepare('SELECT imageA FROM lodges WHERE lodge_id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_imageA']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("imagez/".$imgRow['imageA']);
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM lodges WHERE lodge_id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_imageA']);
		$stmt_delete->execute();
		
		header("Location:lodgeprofile.php");
	}

?>


<?php

	require_once '../con.php';
	if(isset($_GET['delete_imageB']))
    {
		$stmt_select = $DB_con->prepare('SELECT imageB FROM lodges WHERE lodge_id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_imageB']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("imagez/".$imgRow['imageB']);
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM lodges WHERE lodge_id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_imageB']);
		$stmt_delete->execute();
		
		header("Location:lodgeprofile.php");
	}

?>


<?php

	require_once '../con.php';
	if(isset($_GET['delete_imageC']))
    {
		$stmt_select = $DB_con->prepare('SELECT imageC FROM lodges WHERE lodge_id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_imageC']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("imagez/".$imgRow['imageC']);
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM lodges WHERE lodge_id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_imageC']);
		$stmt_delete->execute();
		
		header("Location:lodgeprofile.php");
	}

?>

<?php

	require_once '../con.php';
	if(isset($_GET['delete_imageD']))
    {
		$stmt_select = $DB_con->prepare('SELECT imageD FROM lodges WHERE lodge_id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_imageD']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("imagez/".$imgRow['imageD']);
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM lodges WHERE lodge_id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_imageD']);
		$stmt_delete->execute();
		
		header("Location:lodgeprofile.php");
	}

?>

<?php

	require_once '../con.php';
	if(isset($_GET['delete_header']))
    {
		$stmt_select = $DB_con->prepare('SELECT headerimg FROM lodges WHERE lodge_id =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_header']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("imagez/".$imgRow['headerimg']);
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM lodges WHERE lodge_id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_header']);
		$stmt_delete->execute();
		
		header("Location:lodgeprofile.php");
	}

?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="apple-touch-icon" sizes="57x57" href="../dist/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../dist/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../dist/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../dist/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../dist/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../dist/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../dist/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../dist/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../dist/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="../dist/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="../dist/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="../dist/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="../dist/favicon/favicon-16x16.png">
<link rel="manifest" href="../dist/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="../dist/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

    <script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/flat/green.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
       <!--<link rel="stylesheet" href="../dist/css/licence.css">-->
      <link rel="stylesheet" href="../dist/css/credit.css">
    <link rel="stylesheet" href="../dist/css/credit.css">
    <link rel="stylesheet" href="../dist/css/checkbox.css">
     <link rel="stylesheet" href="../dist/css/slideshow.css">
         <link rel="stylesheet" href="../dist/css/msgPop.css" />
        
<script src="../bootstrap/js/bootstrap.min.js"></script>
     <script language="javascript" type="text/javascript" src="../dist/js/msgPop.js"></script>
    <script language="javascript" type="text/javascript" src="../dist/js/notifs.js"></script>
     <script type="text/javascript" src="../dist/js/date_time.js"></script>
    
    
         <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="../dist/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
          

    <!-- <script type="text/javascript">
	$(document).ready(function(){
		$("#welcome").modal('show');
	});
</script> -->
    

    </script>
       <script language="javascript" type="text/javascript">
  $(function () {
    $( "#tabs" ).tabs();
  });     
</script>
    
<script type="text/javascript">
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    

    
    
    
     <script type="text/javascript">
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
 </script>
    
<script type="text/javascript">

$(document).ready(function() {
  $('.lodge_checkbox').click(function() {
      $(".lodge_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.hotel_checkbox').click(function() {
      $(".hotel_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.resturant_checkbox').click(function() {
      $(".resturant_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.flight_checkbox').click(function() {
      $(".flight_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
  <script type="text/javascript">
    
   $(function(){

$('#lodgel').click(function(){
  $('#lodgelicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    
      <script type="text/javascript">
    
   $(function(){

$('#hotell').click(function(){
  $('#hotellicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    <script type="text/javascript">
    
   $(function(){

$('#restl').click(function(){
  $('#resturantlicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
        <script type="text/javascript">
    
   $(function(){

$('#flightl').click(function(){
  $('#flightlicence').modal('show');
  return false;
})

}); 
  </script> 
        
<!--Licence forms--->
   <script type="text/javascript">
    
   $(function(){

$('#lodlicence').click(function(){
  $('#licencecodeL').modal('show');
  return false;
})

}); 
  </script>
    
     <script type="text/javascript">
    
   $(function(){

$('#hotlicence').click(function(){
  $('#licencecodeH').modal('show');
  return false;
})

}); 
  </script> 
    
      <script type="text/javascript">
    
   $(function(){

$('#restlicence').click(function(){
  $('#licencecodeR').modal('show');
  return false;
})

}); 
  </script> 
    
     <script type="text/javascript">
    
   $(function(){

$('#flicence').click(function(){
  $('#licencecodeF').modal('show');
  return false;
})

}); 
  </script> 
 
    <script src="../../tabs.js"></script>
        
</head>
<body oncontextmenu="return false;"> 
<div class="hold-transition skin-red sidebar-mini">       
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="lodgeprofile.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UL</b>NA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ULNA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 1 message</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                          
                        <img src="images/logos/<?php echo $row['logos'];?>" onerror="this.src='../userfiles/placeholder/imgholder.png'" class="img-circle" >
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>ULNA Welcomes you</p>
                    </a>
                  </li>
                  <!-- end message -->
              
                  
                 
                  
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 1 notification</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  
                 
                 
                 
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 2 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                 
                  <!-- end task item -->
                 
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Upload Some images
                        <small class="pull-right">0%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Tell your clients about you in your profile
                        <small class="pull-right">10%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="images/logos/<?php echo $row['logos'];?>" class="user-image" onerror="this.src='../userfiles/placeholder/imgholder.png'">
              <span class="hidden-xs"> <?php echo $row['cname'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="images/logos/<?php echo $row['logos'];?>" class="img-circle" onerror="this.src='../userfiles/placeholder/imgholder.png'">

                <p>
                   <?php echo $row['cname'] ?>
                  <small>Licence Registered on: <?php echo $row['created_on'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                <a href="lodgelogout.php" class="btn btn-default btn-flat">Sign out</a>
                 
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="images/logos/<?php echo $row['logos'];?>" class="img-circle" onerror="this.src='../userfiles/placeholder/imgholder.png'">
        </div>
        <div class="pull-left info">
          <?php echo $row['cname'] ?>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> <br><br>
          
        </div>
      </div>
      <!--time-->
              <div class="box-success direct-chat-success">
                <div class="direct-chat-msg right">
                  <div class="direct-chat-text">
                   <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
                  </div>
                </div>
          </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
     <ul class="sidebar-menu">
        <li class="header"><span><strong>Advertise Here</strong></span></li>
        <li>
          <div class="box box-solid">
            <!-- /.box-header -->
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="../dist/img/ulnaslide.png" onerror="this.src='../userfiles/placeholder/imgholder.png'">
                    <div class="carousel-caption">
                     <strong>Advertise Here</strong>
                    </div>
                  </div>
                  <div class="item">
                     <img src="../dist/giphy.gif"  onerror="this.src='../userfiles/placeholder/imgholder.png'">

                    <!--<div class="carousel-caption">
                      <strong>Advertise Here</strong>
                    </div>-->
                  </div>
                  <div class="item">
                    <img src="../dist/img/itesslide.jpg"  onerror="this.src='../userfiles/placeholder/imgholder.png'">

                    <!--<div class="carousel-caption">
                     <strong>Advertise Here</strong>
                    </div>-->
                  </div>
                </div>
              </div>
          </div>
        </li>
           <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="../dist/giphy.gif" onerror="this.src='../userfiles/placeholder/imgholder.png'" alt="First slide">

                    <!--<div class="carousel-caption">
                     <strong>Advertise Here</strong>
                    </div>-->
                  </div>
                  <div class="item">
                    <img src="../dist/img/ulnaslide.png" onerror="this.src='../userfiles/placeholder/imgholder.png'" alt="Second slide">
                    <div class="carousel-caption">
                     <strong>Advertise Here</strong>
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/giphy.gif" height="1250" width="835" onerror="this.src='../userfiles/placeholder/imgholder.png'" alt="Third slide">

                    <div class="carousel-caption">
                     <strong>Advertise Here</strong>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
          <li class="active"><a href="../dist/help/help.php"><i class="fa fa-question-circle"></i>Help</a></li>
          <li class="active"><a href="mailto:ulna@ulna.international"><i class="fa fa-question-circle"></i>Contact Us</a></li>
      </ul>
    </section>
  </aside>
    
    
    
    

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <!-- ./col -->
            <ol class="breadcrumb">
        <li><a href="active">Licence</a></li>
        <li class="active">Profile Preview</li>
      </ol>
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence Status</span>
              <span class="info-box-number">100%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                  Ref:<?php echo $row['token'] ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
         
          <div class="col-md-4">
          <div class="box box-widget widget-user">
          <div class="widget-user-header bg-black" style="background: url('images/imagez/<?php echo $row['headerimg'];?>'),url('userfiles/placeholder/imgholder.png') center center;">
              <h3 class="widget-user-username"><b><?php echo $row['cname'] ?></b></h3>
              <h5 class="widget-user-desc"><?php echo $row['sub_type'] ?></h5>
            </div>
              
             
 <div class="widget-user-image">
      <img class="img-circle" src="images/logos/<?php echo $row['logos'];?>" onerror="this.src='../userfiles/placeholder/imgholder.png'"class="img-responsive"/>
            </div>
           <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                    
                  <div class="description-block">
                    <h5 class="description-header"><strong><i class="fa fa-map-marker margin-r-5"></i></strong>Location</h5>
                    <span class="description-text"><h5 class="widget-user-desc">
                        <?php if (empty($row['company_location'])) {
                        echo "Not Provided";} 
                            else {
                        echo $row['company_location'];}?></h5></span>
                  </div>
                 
                </div>
                  
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><strong><i class="fa fa-file margin-r-5" type="button" ></i></strong>About</h5>
                    <span class="description-text"><p><?php if (empty($row['company_shortbio'])) {
                        echo "Not Provided";} 
                            else {
                        echo $row['company_shortbio'];}?></p></span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                      <a href="lodgedetail.php" class="btn btn-success btn-xs" datatoggle="tooltip" title="view your profile">Preview</a>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          
      </div>
      
       
      
        
         
      
  
        <ol class="breadcrumb">
        <li><a href="active">Profile Management</a></li>
        <li class="active">Timeline</li>
      </ol>
        <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                 
                       
           
                
                 <img class="profile-user-img img-responsive img-circle" src="images/logos/<?php echo $row['logos'];?>"onerror="this.src='../userfiles/placeholder/imgholder.png'" class="img-responsive"/>
                <h3 class="profile-username text-center"> <?php echo $row['cname'] ?></h3>
                <p class="text-muted text-center"> <?php echo $row['sub_type'] ?></p> 
              <div class="box-body">

                 <form action="saveprofile.php" method="post">
              <strong><i class="fa fa-book margin-r-5"></i> Short Bio</strong>

              <p class="text-muted">
                  <input type="text" class="form-control"name="company_shortbio" id="company_shortbio" value="<?php echo $row['company_shortbio']?>" >
              </p>
              <hr>  
                   <strong><i class="fa fa-book margin-r-5"></i> long Bio</strong>
                  <textarea class="form-control" type="text" placeholder="Long Bio" name="company_longbio" id="company_longbio" value="<?php echo $row['company_longbio']?>"></textarea>
              </p>
                  
                    <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <input type="text" class="form-control" name="company_location" id="company_location"  value="<?php echo $row['company_location']?>">

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Services</strong>
<!-- Services----------------------------------------------------------------------------->
              <p>
                <span class="label label-danger"><input type="text" class="form-control"  name="service1" id="service1" value="<?php echo $row['service1']?>"></span>
                <span class="label label-info"><input type="text" class="form-control"  name="service2" id="service2" value="<?php echo $row['service2']?>"></span>
                <span class="label label-warning"><input type="text" class="form-control"  name="service3" id="service3" value="<?php echo $row['service3']?>"></span>
                <span class="label label-primary"><input type="text" class="form-control"  name="service4" id="service4" value="<?php echo $row['service4']?>"></span>
              </p>

              <hr>
            </div>
                <input class="btn btn-danger btn-block" type="submit" value="submit">
                </form>
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          
          <!-- /.box -->
        </div>
        <!-- /.col -->
 
        <!-- /.col -->
        <!-- /.col -->
        
       <div class="col-md-9">
          <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Gallery</a></li>
              <li><a href="#timeline" data-toggle="tab">Posts</a></li>
              <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
              
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
               <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="images/logos/<?php echo $row['logos'];?>" onerror="this.src='../userfiles/placeholder/imgholder.png'">
                        <span class="username">
                          <a href="#"><?php echo $row['cname']; ?></a>   
                        </span>
                  </div>
                </div>

  <!-------- header image ------------------------------------------------------------------------------------>              
                <div class="post">
                  <div class="row margin-bottom">
                    <div class="col-sm-6">
                          
                      
                                                           <?php
	$stmt = $DB_con->prepare("SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']);             
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>        
    
                      
                      <a class="btn-sm btn-success " href="images/editheader.php?edit_header=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm image add ?')"><span class="glyphicon glyphicon-plus"></span> </a>
                        
                        <a class="btn-sm btn-info " href="images/editheader.php?edit_header=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm edit image?')"><span class="glyphicon glyphicon-edit"></span> </a>
                       <img  class="img-responsive" src="images/imagez/<?php echo $row['headerimg']; ?>" onerror="this.src='../userfiles/placeholder/imgholder.png'"/>
          
         

                <?php
		}
	}
	else
	{ 
   ?>
                       
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp;Your first Gallery image goes here
            </div>
        </div>                                                        				 
        <?php  
	}
	
?>
                        
                        
                        
                         <script>
    $(function() {
        $('img[data-src-error]').error(function() {
            var o = $(this);
            var errorSrc = o.attr('data-src-error');

            if (o.attr('src') != errorSrc) {
                o.attr('src', errorSrc);
            }
        });
    });
</script>
                    </div>
                      
                      
                      
                      
                      
                      
<!-------- ImageA ------------------------------------------------------------------------------------------------------->
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-6">
                               
                                                           <?php
	$stmt = $DB_con->prepare("SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>        
    
                      
                      <a class="btn-sm btn-success " href="images/editformimageA.php?edit_imageA=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm image add ?')"><span class="glyphicon glyphicon-plus"></span> </a>
                        
                        <a class="btn-sm btn-info " href="images/editformimageA.php?edit_imageA=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm edit image?')"><span class="glyphicon glyphicon-edit"></span> </a>
                       <img src="images/imagez/<?php echo $row['imageA']; ?>"  onerror="this.src='../userfiles/placeholder/imgholder.png'"class="img-responsive"/>
          
          

                <?php
		}
	}
	else
	{ 
   ?>
       <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp;Your first Gallery image goes here
            </div>
        </div>                                                 				 
        <?php  
	}
	
?>
                          <hr>
                                    
                            
                  <!-------- ImageB ------------------------------------------------------------------------------------>            
                                                            <?php
	$stmt = $DB_con->prepare("SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>        
    
                      
                      <a class="btn-sm btn-success" href="images/editformimageB.php?edit_imageB=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm image add ?')"><span class="glyphicon glyphicon-plus"></span> </a>
                        
                        <a class="btn-sm btn-info " href="images/editformimageB.php?edit_imageB=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm edit image?')"><span class="glyphicon glyphicon-edit"></span> </a>
                       <img src="images/imagez/<?php echo $row['imageB']; ?>" onerror="this.src='../userfiles/placeholder/imgholder.png'" class="img-responsive"/>
          
          

                <?php
		}
	}
	else
	{ 
   ?><div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; Your Second Gallery image goes here
            </div>
        </div>                                                    				 
        <?php  
	}
	
?>
                          
                             <hr>
                        </div>
<!-------- ImageC ------------------------------------------------------------------------------------>   
                        <div class="col-sm-6">
                                                                                       <?php
	$stmt = $DB_con->prepare("SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>        
    
                      
                       <a class="btn-sm btn-success " href="images/editformimageC.php?edit_imageC=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm image add ?')"><span class="glyphicon glyphicon-plus"></span> </a>
                        
                        <a class="btn-sm btn-info " href="images/editformimageC.php?edit_imageC=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm edit image?')"><span class="glyphicon glyphicon-edit"></span> </a>
                       <img src="images/imagez/<?php echo $row['imageC']; ?>"onerror="this.src='../userfiles/placeholder/imgholder.png'" class="img-responsive"/>
          

                <?php
		}
	}
	else
	{ 
   ?>
      <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; Your third Gallery image goes here
            </div>
        </div>                                                      				 
        <?php  
	}
	
?>
                          <hr>
                            
        <!-------- ImageD------------------------------------------------------------------------------------>                                                                                  <?php
	$stmt = $DB_con->prepare("SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>        
    
                       
                        <a class="btn-sm btn-success " href="images/editformimageD.php?edit_imageD=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm image add ?')"><span class="glyphicon glyphicon-plus"></span> </a>
                        
                        <a class="btn-sm btn-info " href="images/editformimageD.php?edit_imageD=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('Confirm edit image?')"><span class="glyphicon glyphicon-edit"></span> </a>
                       <img src="images/imagez/<?php echo $row['imageD']; ?>" onerror="this.src='../userfiles/placeholder/imgholder.png'" class="img-responsive"/>
          
          

                <?php
		}
	}
	else
	{ 
   ?>
       <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp;Your fourth Gallery image goes here
            </div>
        </div>                                                      				 
        <?php  
	}
	
?>
                             <hr>
                        </div>
                          
<!-------- Logo------------------------------------------------------------------------------------>            
                          
                          <div class="col-sm-6">
                               <a class="btn-sm btn-success " href="images/addlogo.php"><span class="glyphicon glyphicon-plus"></span> </a> 
                                                            <?php
$stmt = $DB_con->prepare("SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>        
    
                      
                        <a class="btn-sm btn-info " href="images/editlogo.php?edit_id=<?php echo $row['lodge_id']; ?>" title="click for edit" onclick="return confirm('You are about to edit the image ?')"><span class="glyphicon glyphicon-edit"></span> </a> 
                              
                              
                              
                       <!-- <a class="btn-sm btn-danger " href="?delete_id=<?php echo $row['lodge_id']; ?>" title="click for delete" onclick="return confirm('You are about to delete all your the images?Try editing them instead or proceed to delete')"><span class="glyphicon glyphicon-remove-circle"></span></a>
                              -->
                              
                              
                       <img src="images/logos/<?php echo $row['logos'];?>" onerror="this.src='../userfiles/placeholder/imgholder.png'" class="img-responsive"/>
          

                <?php
		}
	}
	else
	{ 
   ?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; Unlock other images by starting with your Logo
            </div>
        </div>                                                      				 
        <?php  
	}
	
?>
                          <hr>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->
                  </div>
    
                </div>
                <!-- /.post -->
              </div>
              <!-- /.timeline -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">Read more</a>
                        <a class="btn btn-danger btn-xs">Delete</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-user bg-aqua"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                      <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                      </h3>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-camera bg-purple"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                      <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                      <div class="timeline-body">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal" action="saveform.php" method="post">
                 <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control"  name="company_email" id="company_email" placeholder="Email" value="<?php echo $row['company_email']?>" required>
                    </div>
                  </div>
                <div class="form-group"> 
                    <label for="inputName" class="col-sm-2 control-label">Phone</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name="company_phone" id="company_phone" placeholder="Phone" value="<?php echo $row['company_phone']?>" required>
                    </div>
                  </div>
                    <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Website</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name="company_website" id="company_website" placeholder="Website" value="<?php echo $row['company_website']?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit"  id="savesettings" value="submit" class="btn btn-danger">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
           
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
      <!-- /.row -->
       <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Check your Internet Connection or Contact Support for Assistance</strong> 
			</div>
            <?php
		}
		?> 
                
                 <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Details saved successfully.</strong> 
			</div>
            <?php
		}
		?> 
  <div class="fixed-action-btn toolbar">
    <a class="btn-floating btn-large red">
      <i class="small fa fa-question"></i>
    </a>
    <ul>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">Report an issue</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">Ulna Help</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">Contact Support</i></a></li>
      <li class="waves-effect waves-light"><a href="#!"><i class="material-icons">Rate Ulna</i></a></li>
    </ul>
  </div>
  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2015-2017 <a href="http://iteslink.com">ULNA</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i href="logout.php" class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  
  <div class="control-sidebar-bg"></div>
</div>
    </div>
<!-- ./wrapper -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- jQuery 2.2.3 -->
  <script src="../plugins/morris/morris.js"></script>  
  <script src="../plugins/morris/morris.min.js"></script> 

<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jQueryUI/jquery-ui.js"></script>
<script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jQueryUI/jquery-ui.js"></script>
<script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
    
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script src="../dist/js/licence.js"></script>
    <script src="../dist/js/credit.js"></script>

   
    
</body>
</html>
