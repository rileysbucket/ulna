<?php
session_start();
error_reporting( ~E_NOTICE );
if (!isset($_SESSION['lodge'])) {
    header("Location:lodge.php");
}
require_once'../../con.php';
	if(isset($_POST['btnsave']))
	{
        $id=$_SESSION['lodge'];
		$imgFile = $_FILES['user_imageheader']['name'];
		$tmp_dir = $_FILES['user_imageheader']['tmp_name'];
		$imgSize = $_FILES['user_imageheader']['size'];
		
		
		if(empty($imgFile)){
			$errMSG = "Please Select Image File.";
		}
		
		else
		{
			$upload_dir = 'imagez/'; 
	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); 
		
			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); 
		
			// rename uploading image
			$headerimg = rand(1000,1000000).".".$imgExt;
				
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){			
				// Check file size '5MB'
				if($imgSize < 5000000)				{
					move_uploaded_file($tmp_dir,$upload_dir.$headerimg);
				}
				else{
					$errMSG = "Sorry, your file is too large.";
				}
			}
			else{
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";		
			}
		}
		// if no error occured, continue ....
		if(!isset($errMSG))
		{
            
            $stmt = $DB_con->prepare('UPDATE lodges 
									     SET   
										     headerimg=:imgH 
								       WHERE lodge_id=:uid');
			$stmt->bindParam(':upic',$headerimg);
			$stmt->bindParam(':uid',$id);
       
            
			if($stmt->execute())
			{
				$successMSG = "new record succesfully inserted ...redirecting you to profile";
				header("refresh:2;../lodgeprofile.php"); // redirects image view page after 5 seconds.
			}
			else
			{
				$errMSG = "error while inserting....";
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload, Insert, Update, Delete an Image using PHP MySQL - Coding Cage</title>

 <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

</head>
<body>



<div class="container">


	<div class="page-header">
    	<h1 class="h2">Add Header</a></h1>
    </div>
    

	<?php
	if(isset($errMSG)){
			?>
            <div class="alert alert-danger">
            	<span class="glyphicon glyphicon-info-sign"></span> <strong><?php echo $errMSG; ?></strong>
            </div>
            <?php
	}
	else if(isset($successMSG)){
		?>
        <div class="alert alert-success">
              <strong><span class="glyphicon glyphicon-info-sign"></span> <?php echo $successMSG; ?></strong>
        </div>
        <?php
	}
	?>   

<form method="post" enctype="multipart/form-data" class="form-horizontal">
	    
	<table class="table table-bordered table-responsive">
    <tr>
    	<td><label class="control-label">Header Image</label></td>
        <td><input class="input-group" type="file" name="user_imageheader" accept="image/*" /></td>
    </tr>
    
    <tr>
        <td colspan="2"><button type="submit" name="btnsave" class="btn btn-success">
        <span class="glyphicon glyphicon-save"></span> &nbsp; save
        </button>
    <a class="btn btn-default" href="../lodgeprofile.php"> <span class="glyphicon glyphicon-backward"></span> cancel </a>
        </td>
    </tr>
    </table>
    
</form>
</div>



	


<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>


</body>
</html>