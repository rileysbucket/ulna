create table names (
  id INT (6) unsigned auto_increment PRIMARY KEY,
  name varchar(200) not null
);
insert into names (name) VALUES ('Johhny bravo');
insert into names (name) VALUES ('Dwayne Jhonson');
insert into names (name) VALUES ('Dexter');
insert into names (name) VALUES ('Pakalu Papito');
insert into names (name) VALUES ('Sachin Tendulkar');
insert into names (name) VALUES ('Aman Kharbanda');
insert into names (name) VALUES ('Farhan Akhtar');
insert into names (name) VALUES ('Chris Martin');
insert into names (name) VALUES ('Charlie Puth');
insert into names (name) VALUES ('John Cena');