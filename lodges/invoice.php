<?php
session_start();
//$con=mysqli_connect("localhost:3306","ulna","Ul2017@$#","ulna_db");
$con=mysqli_connect("localhost","root","Genious2016","ulna_db");
$res = mysqli_query($con, "SELECT * FROM lodges"); 
$row = mysqli_fetch_array($res);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA|Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
    <body>
 <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-default'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Thank you for your submission.We have generated an invoice for you.Proceed to make your payment inorder to your ULNA Licence Token</strong> 
			</div>
            <?php
		}
		?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <small><b>Reference Number: <?php echo $row['reference'] ?></b></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="start.php"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> <b>NB:</b></h4>
        <p>For EFT Please use the invoice number as your reference</p>
      </div>
    </div>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <img src ="dist/img/logo3B.png" style=height:70px ></i> ULNA
            <small class="pull-right"><strong>Date:</strong><?php echo date('Y-m-d');?><br></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>ULNA</strong><br>
            Jenner St 64<br>
            P.O.Box 96499
            Windhoek, Namibia<br>
            Email: info@ulna.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>Company:</strong><?php echo $row['cname'] ?><br>
             <strong>Rep:</strong><?php echo $row['crep'] ?><br>
            <strong>Email:</strong><?php echo $row['company_email'] ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice <?php echo $row['reference'] ?></b><br>
          <br>
          <b>Order ID:</b> <?php echo $row['lodge_id'] ?><br>
          <b>Payment Due:</b> <?php echo date('Y-m-d');?><br>
          <b>Account:</b> <?php echo $row['reference'] ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Product</th>
              <th>Serial #</th>
              <th>Description</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>ULNA-Lodge Licence</td>
              <td>455-981-220</td>
              <td>ULNA Lodge Licence</td>
              <td>N$425</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <img src="../../dist/img/credit/visa.png" alt="Visa">
          <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="../../dist/img/credit/american-express.png" alt="American Express">
          <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            
Amount to be charged to your credit/debit card:<strong> R 500.00</strong><br>
              <strong>Payments can be made via EFT</strong><br>
              
<br><strong>Account Details:</strong><br>
              
              
              <strong>Bank:</strong>First National Bank<br>
            <strong>Branch Code:</strong>280272<br>
            <strong>Account Type:</strong>Business Savings Account<br>
            <strong>Account Number:</strong>62260254695<br>
            
            Please user your refference number for as your payment refference
            
<!--Please enter your credit card details below and press "Pay now" to proceed:
VISA MASTER-->

          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due 2/22/2014</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>N$425.00</td>
              </tr>
              <tr>
                <th>Tax (15%)</th>
                <td>N$75</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td>N$0</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>N$500</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Save as PDF
          </button>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
    </body>
</html>
