A Pen created at CodePen.io. You can find this one at http://codepen.io/somethingkindawierd/pen/ncvGf.

 Show your current location in Leaflet.js/OpenStreetMap and draw a circle around the point.

Utilizes geoplugin.net & the browser's built in geolocation.