Introduction
============

**ULNA** -- is a fully responsive Hotel and Lodge Management System. Based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework.
Developed using Php and MySql Database. 


git clone https://bitbucket.org/rileysbucket/ulna.git
```

**Bower**

```
bower install ulna
```

**npm**

```
npm install --save ulna
```

License
-------
Licenced under ITES Trademark



