<?php

session_start();
$con=mysqli_connect("localhost","root","Genious2016","ulna");
if (!isset($_SESSION['lodge'])) {
    header("Location:lodge.php");
}
$token = "";
$res = mysqli_query($con, "SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
$row = mysqli_fetch_array($res);
if ($row['token'] == ($token)) {
    $_SESSION['lodge'] = $row['lodge_id'];
    header('location:lodge.php');
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
     <link rel="stylesheet" href="../dist/css/j.min.css">
    
     <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="../dist/css/materialize.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
    
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../dist/css/ribbon.css">
         <script type="text/javascript" src="../dist/js/date_time.js"></script>
    <link href="../dist/assets/css/custom.css" rel="stylesheet" type="text/css">
    
  
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    
<script type="text/javascript">
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    


    
     <script type="text/javascript">
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
 </script>
    
</head>
<body>


       
      
    
<div class="hold-transition skin-red sidebar-mini">       
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="lodge.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UL</b>NA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ULNA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 1 message</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Welcome to ULNA</p>
                    </a>
                  </li>
                  <!-- end message -->
                
            
                </ul>
              </li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 0 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <!--<li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>-->
                </ul>
              </li>
              
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 0 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  
                  <!-- end task item -->
                 
                
                 
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../userfiles/avatars/default.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo $row['cname'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../userfiles/avatars/default.jpg"class="img-circle" alt="User Image">

                <p>
                   <?php echo $row['cname'] ?>
                  <small>Licence Registered on: <?php echo $row['created_on'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
             <!-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="lodgeprofile.php" class="btn btn-default btn-flat" data-toggle="tooltip" title="Your Profile">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="lodgelogout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <?php echo $row['cname'] ?>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> <br><br>
          
        </div>
      </div>
      <!--time-->
              <div class="box-success direct-chat-success">
                <div class="direct-chat-msg right">
                  <div class="direct-chat-text">
                   <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
                  </div>
                </div>
          </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><span>Advertise Here</span></li>
        <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="../dist/img/photo1.png" alt="First slide">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo2.png" alt="Second slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo3.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
           <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="../dist/img/photo3.jpg" alt="First slide">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo3.jpg" alt="Second slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo4.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
          <li class="active"><a href="../dist/help/help.php"><i class="fa fa-question-circle"></i>Help</a></li>
          <li class="active"><a href="mailto:ulna@ulna.international"><i class="fa fa-question-circle"></i>Contact Us</a></li>
      </ul>
    </section>
  </aside>

    
    
    
    <div class="modal" id="dialog" >
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="title"></h4>
          <input class="modal-title"type="text" id="title"/>
        <div class="external-event bg-fuchsia" id="status"></div>  
	  </div>
	  <div class="modal-body">
		<h3 id="client"></h3>
          <hr>
          From:<h3 id="startTime"></h3>
           <h3 id="endTime"></h3>
	  </div>
	  <div class="modal-footer">
          
		<p>powered by ULNA<img src="../dist/img/logo3B.png" style=height:40px;/></p>
          
	  </div>
	</div>
  </div>
</div>
    
    
    
    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
 
    <!-- Main content -->
    <section class="content">
        
        
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <!-- ./col -->
          
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence Status</span>
              <span class="info-box-number">100%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                  Ref:<?php echo $row['token'] ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
        
        
       <!--  <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search for a lodge,hotel,resturant or flights to plaace a reservation...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.row -->

    </section>
      
      
      
  
      </script>     
      
      <section class="content">
          
      <div class="row">
        <div class="col-md-3">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h4 class="box-title">All Rooms</h4>
            </div>
              
              <table style="width:100%" >
                  
                  
                  <thead>
                            <tr>

                                <th colspan="1" rowspan="1" style="width: 180px;" tabindex="0"><div class="box-header with-border"> <h4 class="box-title">Rooms</h4></div></th>

                                <th colspan="1" rowspan="1" style="width: 220px;" tabindex="0"><div class="box-header with-border"> <h4 class="box-title">Capacity</h4></div></th>

                                <th colspan="1" rowspan="1" style="width: 288px;" tabindex="0"><div class="box-header with-border"> <h4 class="box-title">Status</h4></div></th>
                            </tr>
                        </thead>
                  
    
                      </div>
                  </div>
                 
              
            <tr>
                
                <td>
                <?php
$id = "id";
$con=mysqli_connect("localhost","root","Genious2016","ulna");
$res = mysqli_query($con, "SELECT * FROM sossegorooms"); 

                  
                   while($row = $res->fetch_assoc()) {
                       extract($row);

?>    
                
                         
                    
                    
            <div class="box-body">
              <div id="external-events">
                  
             <div class="external-event bg-green"><?php echo $row['name'];?></div>
                   
                 
            
                <?php
		}
	
	
   ?>
                       

                
           
                 
              </div>
            </div>
                </td>              
       
                
                <td>
                <?php
$id = "id";
                    $capacity = "capacity";
$con=mysqli_connect("localhost","root","Genious2016","ulna");
$res = mysqli_query($con, "SELECT * FROM sossegorooms"); 

                  
                   while($row = $res->fetch_assoc()) {
                       extract($row);

?>    
                
                         
                    
                    
            <div class="box-body">
              <div id="external-events">
                  
             <div class="external-event bg-green"><?php echo $row['capacity'];?></div>
                   
                 
            
                <?php
		}
	
	
   ?>
                       

                
           
                 
              </div>
            </div>
                </td>              
       
              <td>
                <?php
$id = "id";
$con=mysqli_connect("localhost","root","Genious2016","ulna");
$res = mysqli_query($con, "SELECT * FROM sossegorooms"); 

                  
                   while($row = $res->fetch_assoc()) {
                       extract($row);

?>    
                
                         
                    
                    
            <div class="box-body">
              <div id="external-events">
                  
             <div class="external-event bg-red"><?php echo $row['status'];?></div>
                   
                 
            
                <?php
		}
	
	
   ?>
                       

                
           
                 
              </div>
            </div>
                </td>              
       
               </tr>  
                 
                 </table>  

          <style>
          
          .fa-arrow-circle-down {
  color: green;
              padding: 4px;
}
          </style>
<img src="../dist/img/trashcan.png" id="trash" alt="" height="48" width="48">
          <tr>
               <div class="box-header with-border">
              <h4 class="box-title" data-toggle="tooltip" title="Close">Edit Room Names<i class="fa fa-arrow-circle-down" aria-hidden="true" ></i>
</h4>
            </div>
            
             
                <tbody>
                         <div id="live_data"></div>      
                        </tbody>

			
          </div>
          
          
          
          
          
          
          
      
             
              
                              
              
        
      
         <script>  
 $(document).ready(function(){  
      function fetch_data()  
      {  
           $.ajax({  
                url:"select.php",  
                method:"POST",  
                success:function(data){  
                     $('#live_data').html(data);  
                }  
           });  
      }  
      fetch_data();  
      $(document).on('click', '#btn_add', function(){  
           var name = $('#name').text();  
           var capacity = $('#capacity').text();  
           if(first_name == '')  
           {  
                alert("Enter  Name");  
                return false;  
           }  
           if(last_name == '')  
           {  
                alert("Enter Name");  
                return false;  
           }  
           $.ajax({  
                url:"insert.php",  
                method:"POST",  
                data:{name:name, capacity:capacity},  
                dataType:"text",  
                success:function(data)  
                {  
                     alert(data);  
                     fetch_data();  
                }  
           })  
      });  
      function edit_data(id, text, column_name)  
      {  
           $.ajax({  
                url:"edit.php",  
                method:"POST",  
                data:{id:id, text:text, column_name:column_name},  
                dataType:"text",  
                success:function(data){  
                     alert(data);  
                }  
           });  
      }  
      $(document).on('blur', '.name', function(){  
           var id = $(this).data("id1");  
           var first_name = $(this).text();  
           edit_data(id, first_name, "name");  
      });  
      $(document).on('blur', '.capacity', function(){  
           var id = $(this).data("id2");  
           var last_name = $(this).text();  
           edit_data(id,last_name, "capacity");  
      });  
      $(document).on('click', '.btn_delete', function(){  
           var id=$(this).data("id3");  
           if(confirm("Are you sure you want to delete this?"))  
           {  
                $.ajax({  
                     url:"delete.php",  
                     method:"POST",  
                     data:{id:id},  
                     dataType:"text",  
                     success:function(data){  
                          alert(data);  
                          fetch_data();  
                     }  
                });  
           }  
      });  
 });  
          </script> 
          
          
          

          
          
          
          
         
          
          
          
          
          
          
          
          
          
          <!-- /. box -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Create New Rooms</h3>
            </div>
              
              
              
            
            <div class="box-body">
              <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                <ul class="fc-color-picker" id="color-chooser">
                  <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                  <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                </ul>
              </div>
              <!-- /btn-group -->
              <div class="input-group">
                  <div class="ribbon-wrapper"><div class="glow">&nbsp;</div>
		<div class="ribbon-front">
			Coming Soon!
          
		</div>
		<div class="ribbon-edge-topleft"></div>
		<div class="ribbon-edge-topright"></div>
		<div class="ribbon-edge-bottomleft"></div>
		<div class="ribbon-edge-bottomright"></div>
	</div>
                   <input id="new-room" type="text" class="form-control" placeholder="Room"  name="new-room" required>
                       <input id="new-capacity" type="number" class="form-control" min="1" max="5" placeholder="Capacity" required>
                      
                       <input id="new-status" type="hidden" class="form-control" min="1" max="5" placeholder="status">
                 
                 <!-- <button id="add-new-room" name="add-newroom" type="submit" class="btn btn-primary btn-flat">Add Room</button> -->
                
                <!-- /btn-group -->
              </div>
              <!-- /input-group -->
            </div>
                  
          </div>
          
          
          
          
        </div>
    
    
     
         
          
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-danger">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
          
          
        
      <!-- /.row -->
    </section>
      
      
      
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2015-2017 <a href="http://iteslink.com">ULNA</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i href="logout.php" class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
    </div>
 
<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/fullcalendar/fullcalendar.min.js"></script>
  <script src="../dist/assets/js/bootstrap-editable.js" type="text/javascript"></script> 
<!-- Page specific script -->


<script>

	$(document).ready(function() {

		var zone = "05:30";  //Change this to your timezone

	$.ajax({
		url: 'process.php',
        type: 'POST', // Send post data
        data: 'type=fetch',
        async: false,
        success: function(s){
        	json_events = s;
        }
	});


	var currentMousePos = {
	    x: -1,
	    y: -1
	};
		jQuery(document).on("mousemove", function (event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

		/* initialize the external events
		-----------------------------------------------------------------*/
$('#external-events .external-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});



		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			events: JSON.parse(json_events),
			//events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
			utc: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true, 
			slotDuration: '00:30:00',
            
            
			eventReceive: function(event){
				var title = event.title;
				var start = event.start.format("YYYY-MM-DD[T]HH:mm:SS");
				$.ajax({
		    		url: 'process.php',
		    		data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
		    		type: 'POST',
		    		dataType: 'json',
		    		success: function(response){
		    			event.id = response.eventid;
		    			$('#calendar').fullCalendar('updateEvent',event);
		    		},
		    		error: function(e){
		    			console.log(e.responseText);

		    		}
		    	});
				$('#calendar').fullCalendar('updateEvent',event);
				console.log(event);
			},
            
			eventDrop: function(event, delta, revertFunc) {
		        var title = event.title;
		        var start = event.start.format();
		        var end = (event.end == null) ? start : event.end.format();
		        $.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
		    eventClick: function(event, jsEvent, view,element) {
		    	console.log(event.id);
		          var title = prompt('Room Title:',event.title,
                                     
                                     
                                     { buttons: { Ok: true, Cancel: false} });
               
		          if (title){
		              event.title = title;
		              console.log('type=changetitle&title='+title+' &eventid='+event.id);
		              $.ajax({
				    		url: 'process.php',
				    		data: 'type=changetitle&title='+title+'&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){	
				    			if(response.status == 'success')			    			
		              				$('#calendar').fullCalendar('updateEvent',event);
				    		},
				    		error: function(e){
                                
				    			alert('Error processing your request: '+e.responseText);
				    		}
				    	});
		          }
			},
            
           
        
            
              eventRender: function (event, element) {
        element.attr('href', 'javascript:void(0);');
        element.click(function() {
            $("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
            $("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
            $("#title").html(event.title);
            $("#client").html(event.client);
            $("#status").html(event.status);
            $("#dialog").dialog({ modal: true, title: event.title, width:350});
        });
        },
            
            
            
            
            
            
			eventResize: function(event, delta, revertFunc) {
				console.log(event);
				var title = event.title;
				var end = event.end.format();
				var start = event.start.format();
		        $.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
			eventDragStop: function (event, jsEvent, ui, view) {
			    if (isElemOverDiv()) {
			    	var con = confirm('Are you sure to delete this reservation permanently?');
			    	if(con == true) {
						$.ajax({
				    		url: 'process.php',
				    		data: 'type=remove&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){
				    			console.log(response);
				    			if(response.status == 'success'){
				    				$('#calendar').fullCalendar('removeEvents');
            						getFreshEvents();
            					}
				    		},
				    		error: function(e){	
				    			alert('Error processing your request: '+e.responseText);
				    		}
			    		});
					}   
				}
			}
		});

	function getFreshEvents(){
		$.ajax({
			url: 'process.php',
	        type: 'POST', // Send post data
	        data: 'type=fetch',
	        async: false,
	        success: function(s){
	        	freshevents = s;
	        }
		});
		$('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
	}


	function isElemOverDiv() {
        var trashEl = jQuery('#trash');

        var ofs = trashEl.offset();

        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);

        if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
            currentMousePos.y >= y1 && currentMousePos.y <= y2) {
            return true;
        }
        return false;
    }

        
        
      /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
      e.preventDefault();
      //Save color
      currColor = $(this).css("color");
      //Add color effect to button
      $('#add-new-room').css({"background-color": currColor, "border-color": currColor});  
    });
      
    $("#add-new-room").click(function (e) {
      e.preventDefault();
      var val = $("#new-room").val();
        var con = confirm('We are working hard to bring this functionality to you.');
      if (val.length == 0) {
        return;
      }
      //Create events
      var event = $("<div />");
      event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
      event.html(val);
      $('#external-events').prepend(event);
      //Add draggable funtionality
       ini_events(event);
   //Remove event from text input
    $("#new-room").val("");
    });
         
        
        
        
	});



</script>

<script type="text/javascript">
jQuery(document).ready(function() {  
        $.fn.editable.defaults.mode = 'popup';
        $('.xedit').editable();	
		$(document).on('click','.editable-submit',function(){
			var x = $(this).closest('td').children('span').attr('id');
			var y = $('.input-sm').val();
            var p = $('.input-sm').val();
			var z = $(this).closest('td').children('span');
			$.ajax({
				url: 'processrooms.php?id='+x+'&data='+y+'&capacity='+p,
				type: 'GET',
				success: function(s){
					if(s == 'status'){
					$(z).html(y);
                        $(z).html(p);
                    }
					if(s == 'error') {
					alert('Error Processing your Request!');}
				},
				error: function(e){
					alert('Error Processing your Request!!');
				}
			});
		});
});
</script>


    
</body>
</html>
