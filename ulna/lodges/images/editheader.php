
 <?php
session_start();
error_reporting( ~E_NOTICE );
if (!isset($_SESSION['lodge'])) {
    header("Location:../lodgeprofile.php");
}
require_once '../../con.php';

	if(isset($_GET['edit_header']) && !empty($_GET['edit_header']))
	{
		$id=$_SESSION['lodge'];
		$stmt_edit = $DB_con->prepare('SELECT headerimg FROM lodges WHERE lodge_id =:uid');
		$stmt_edit->execute(array(':uid'=>$id));
		$edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
		extract($edit_row);
	}
	else
	{
		header("Location:../lodgeprofile.php");
	}
	
	
	
	if(isset($_POST['btn_save_updates']))
	{	
		$imgFile = $_FILES['user_imageheader']['name'];
		$tmp_dir = $_FILES['user_imageheader']['tmp_name'];
		$imgSize = $_FILES['user_imageheader']['size'];
					
		if($imgFile)
		{
			$upload_dir = 'imagez/'; // upload directory	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			$headerimg = rand(1000,1000000).".".$imgExt;
			if(in_array($imgExt, $valid_extensions))
			{			
				if($imgSize < 5000000)
				{
					unlink($upload_dir.$edit_row['headerimg']);
					move_uploaded_file($tmp_dir,$upload_dir.$headerimg);
				}
				else
				{
					$errMSG = "Sorry, your file is too large it should be less then 5MB";
				}
			}
			else
			{
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";		
			}	
		}
		else
		{
			// if no image selected the old image remain as it is.
			$headerimg = $edit_row['headerimg']; // old image from database
		}	
						
		
		// if no error occured, continue ....
		if(!isset($errMSG))
		{  
            $stmt = $DB_con->prepare('UPDATE lodges 
									     SET   
										     headerimg=:imgH 
								       WHERE lodge_id=:uid');
			$stmt->bindParam(':imgH',$headerimg);
			$stmt->bindParam(':uid',$id);
        
				
			if($stmt->execute()){
				?>
                <script>
				alert('Successfully Updated ...');
				window.location.href='../lodgeprofile.php';
				</script>
                <?php
			}
			else{
				$errMSG = "Sorry Data Could Not Updated !";
			}
		
		}
		
						
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ULNA</title>

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap/css/bootstrap-theme.min.css">

<!-- custom stylesheet -->
<link rel="stylesheet" href="style.css">

<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="jquery-1.11.3-jquery.min.js"></script>
</head>
<body>




<div class="container">


	<div class="page-header">
    	<h1 class="h2">Update Header Image<!--<a class="btn btn-default" href="index.php"></a>--></h1>
    </div>

<div class="clearfix"></div>

<form method="post" enctype="multipart/form-data" class="form-horizontal">
	
    
    <?php
	if(isset($errMSG)){
		?>
        <div class="alert alert-danger">
          <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
        </div>
        <?php
	}
	?>
   
    
	<table class="table table-bordered table-responsive">
	
   
    <tr>
    	<td><label class="control-label">Header</label></td>
        <td>
        	<p><img src="imagez/<?php echo $headerimg;?>" height="150" width="150" /></p>
        	<input class="input-group" type="file" name="user_imageheader" accept="image/*" />
        </td>
    </tr>
    
    <tr>
        <td colspan="2"><button type="submit" name="btn_save_updates" class="btn btn-success">
        <span class="glyphicon glyphicon-save"></span> Update
        </button>
        
        <a class="btn btn-default" href="../lodgeprofile.php"> <span class="glyphicon glyphicon-backward"></span> cancel </a>
        
        </td>
    </tr>
    
    </table>
    
</form>
</div>
</body>
</html>