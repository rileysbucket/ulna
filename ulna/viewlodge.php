<?php
session_start();
require_once 'class.user.php';
$user_home = new USER();

if(!$user_home->is_logged_in())
{
	$user_home->redirect('index.php');
}
$stmt = $user_home->runQuery("SELECT * FROM tbl_users WHERE userID=:uid");
$stmt->execute(array(":uid"=>$_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/green.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    
      <link rel="stylesheet" href="dist/css/credit.css">
    <link rel="stylesheet" href="dist/css/credit.css">
    <link rel="stylesheet" href="dist/css/checkbox.css">
     <link rel="stylesheet" href="dist/css/slideshow.css">
         <link rel="stylesheet" href="dist/css/msgPop.css" />
        
<script src="bootstrap/js/bootstrap.min.js"></script>
     <script language="javascript" type="text/javascript" src="dist/js/msgPop.js"></script>
    <script language="javascript" type="text/javascript" src="dist/js/notifs.js"></script>
     <script type="text/javascript" src="dist/js/date_time.js"></script>
    

    <!-- <script type="text/javascript">
	$(document).ready(function(){
		$("#welcome").modal('show');
	});
</script> -->
    
      <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="dist/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
    
    
    
    
       <script language="javascript" type="text/javascript">
  $(function () {
    $( "#tabs" ).tabs();
  });     
</script>
    
    
     
    
     
    
<script type="text/javascript">
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    

    
    
    
     <script type="text/javascript">
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
 </script>
    
<script type="text/javascript">

$(document).ready(function() {
  $('.lodge_checkbox').click(function() {
      $(".lodge_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.hotel_checkbox').click(function() {
      $(".hotel_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.resturant_checkbox').click(function() {
      $(".resturant_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.flight_checkbox').click(function() {
      $(".flight_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
  <script type="text/javascript">
    
   $(function(){

$('#lodgel').click(function(){
  $('#lodgelicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    
      <script type="text/javascript">
    
   $(function(){

$('#hotell').click(function(){
  $('#hotellicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    <script type="text/javascript">
    
   $(function(){

$('#restl').click(function(){
  $('#resturantlicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
        <script type="text/javascript">
    
   $(function(){

$('#flightl').click(function(){
  $('#flightlicence').modal('show');
  return false;
})

}); 
  </script> 
        
<!--Licence forms--->
   <script type="text/javascript">
    
   $(function(){

$('#lodlicence').click(function(){
  $('#licencecodeL').modal('show');
  return false;
})

}); 
  </script>
    
     <script type="text/javascript">
    
   $(function(){

$('#hotlicence').click(function(){
  $('#licencecodeH').modal('show');
  return false;
})

}); 
  </script> 
    
      <script type="text/javascript">
    
   $(function(){

$('#restlicence').click(function(){
  $('#licencecodeR').modal('show');
  return false;
})

}); 
  </script> 
    
     <script type="text/javascript">
    
   $(function(){

$('#flicence').click(function(){
  $('#licencecodeF').modal('show');
  return false;
})

}); 
  </script> 
    
    
    
       
    
  
    
    
    
      
 
    <script src="../../tabs.js"></script>
      
   
</head>
<body class="viewdetail">

  
<div class="hold-transition skin-red sidebar-mini">       
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UL</b>NA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ULNA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg"  class="img-circle" onerror="this.src='userfiles/placeholder/imgholder.png'" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg"  class="img-circle" onerror="this.src='userfiles/placeholder/imgholder.png'" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'" class="img-circle"   alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'"  class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="userfiles/avatars/default.jpg"  class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo $row['userName'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="userfiles/avatars/default.jpg"class="img-circle" onerror="this.src='userfiles/placeholder/imgholder.png'" alt="User Image">

                <p>
                   <?php echo $row['userName'] ?>
                  <small>Join Date: <?php echo $row['join_date'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
             
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="images/logos/<?php echo $row['logos'];?>" onerror="this.src='userfiles/placeholder/imgholder.png'"  class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <?php echo $row['userName'] ?>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> <br><br>
          
        </div>
      </div>
      <!--time-->
              <div class="box-success direct-chat-success">
                <div class="direct-chat-msg right">
                  <div class="direct-chat-text">
                   <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
                  </div>
                </div>
          </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><span>Advertise Here</span></li>
        <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="dist/img/photo1.png" onerror="this.src='userfiles/placeholder/imgholder.png'">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo2.png"  onerror="this.src='userfiles/placeholder/imgholder.png'">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo3.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
           <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="dist/img/photo3.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo3.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo4.jpg" onerror="this.src='userfiles/placeholder/imgholder.png'">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
          <li class="active"><a href="index.php"><i class="fa fa-question-circle"></i>Help</a></li>
          <li class="active"><a href="index.php"><i class="fa fa-question-circle"></i>Contact Us</a></li>
      </ul>
    </section>
  </aside>
    
<div class="example-modal" id="memberModal">
        <div class="modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
    
    
    
    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
 
    <!-- Main content -->
    <section class="content">


              <div class="page-wrap">
  <div id="lodge-container"></div>
</div>
        
        

 <!--<img src="lodges/images/logos/{{photo}}">-->

   
<!--{{#with lodges.[0]}} -->
   

 <script id="lodgeTemplate" type="text/x-handlebars-template">

  {{debug lodgeTemplate}}
{{#each lodges}}

<div class="box-body">
               <div class="col-md-6">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="lodges/images/logos/{{this.lodge_id}}. {{this.logos}}" onerror="this.src='userfiles/placeholder/imgholder.png'">
                <span class="username"><a href="#">{{cname}}</a></span>
                <span class="description">Shared publicly - 7:30 PM Today</span>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive pad" src="lodges/images/imagez/{{headerimg}}" onerror="this.src='userfiles/placeholder/imgholder.png'">
              <p>{{company_shortbio}}</p>
              <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-book"></i> Make Reservation</button>
              <button type="button" class="btn btn-success btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
              <span class="pull-right text-muted">127 likes</span>
            </div>
            
            <!-- /.box-body -->
            <div class="box-footer box-comments">
              <div class="box-comment">
                <!-- User image -->
                <div class="comment-text">
                      <span class="username">
                        About
                      </span><!-- /.username -->
                  <p>{{company_longbio}}</p>
                </div>
                <!-- /.comment-text -->
              </div>
              <!-- /.box-comment -->
              <div class="box-comment">
                <!-- User image -->
               <div class="comment-text">
                      <span class="username">
                        Services
                      </span><!-- /.username -->
                   <p>
                <span class="label label-danger">{{service1}}</span>
                <span class="label label-success">{{service2}}</span>
                <span class="label label-info">{{service3}}</span>
                <span class="label label-warning">{{service4}}</span>
                <span class="label label-primary">{{service5}}</span>
              </p>
                </div>
                <!-- /.comment-text -->
              </div>
              <!-- /.box-comment -->
            </div>
            <!-- /.box-footer -->
            <div class="box-footer">
             <span class="username">
                        Location:
                      </span>
                  {{company_location}}
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="lodges/images/logos/{{logos}}"  onerror="this.src='userfiles/placeholder/imgholder.png'">
                <span class="username"><a href="#">{{cname}} Gallery</a></span>
              </div>
            </div>
            
            
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive pad" src="lodges/images/imagez/{{headerimg}}"   onerror="this.src='userfiles/placeholder/imgholder.png'">
            </div>
            <!-- /.box-body -->
            
            
            <!-- /.box-footer -->
            <div class="box-footer">
             <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="lodges/images/logos/{{logos}}" onerror="this.src='userfiles/placeholder/imgholder.png'">
                       
                          <span class="description">{{cname}}</span>
                  </div>
                  <!-- /.user-block -->
                  <div class="row margin-bottom">
                    <div class="col-sm-6">
                      <img class="img-responsive" src="lodges/images/imagez/{{imageA}}" onerror="this.src='userfiles/placeholder/imgholder.png'">
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-6">
                          <img class="img-responsive" src="lodges/images/imagez/{{imageA}}"  onerror="this.src='userfiles/placeholder/imgholder.png'">
                          <br>
                          <img class="img-responsive" src="lodges/images/imagez/{{imageB}}"   onerror="this.src='userfiles/placeholder/imgholder.png'">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                          <img class="img-responsive" src="lodges/images/imagez/{{imageC}}"  onerror="this.src='userfiles/placeholder/imgholder.png'">
                          <br>
                          <img class="img-responsive round" src="lodges/images/imagez/{{imageD}}"  onerror="this.src='userfiles/placeholder/imgholder.png'">
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <ul class="list-inline">
                    <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                    </li>
                 </ul>
                </div>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
           
              
              
              <!--Body inner end-->
              
              
            </div>
            <!-- /.box-body -->
          </div>

{{/each}}

  
</script>
   <script src="dist/handlebars/dist/handlebars.js"></script>
    <script src="request.js"></script>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2015-2017 <a href="http://iteslink.com">ULNA</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i href="logout.php" class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
    </div>
<!-- ./wrapper -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- jQuery 2.2.3 -->
  <script src="plugins/morris/morris.js"></script>  
  <script src="plugins/morris/morris.min.js"></script> 

<!-- jQuery UI 1.11.4 -->
<script src="plugins/jQueryUI/jquery-ui.js"></script>
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jQueryUI/jquery-ui.js"></script>
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
    
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="dist/js/licence.js"></script>
    <script src="dist/js/credit.js"></script>

   
    
</body>
</html>
