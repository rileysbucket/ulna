<?php
session_start();
require_once 'class.user.php';

$reg_user = new USER();

//if($reg_user->is_subscribed()!="")
//{
	//$reg_user->redirect('home.php');
//}


if(isset($_POST['btn-signup']))
{
	$uname = trim($_POST['txtuname']);
	$email = trim($_POST['txtemail']);
	$upass = trim($_POST['txtpass']);
	$code = md5(uniqid(rand()));
	
	$stmt = $reg_user->runQuery("SELECT * FROM tbl_users WHERE userEmail=:email_id");
	$stmt->execute(array(":email_id"=>$email));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($stmt->rowCount() > 0)
	{
		$msg = "
		      <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
					<strong>Sorry !</strong>  this email allready exists , Please Try another one 
			  </div>
			  ";
	}
	else
	{
		if($reg_user->register($uname,$email,$upass,$code))
		{			
			$id = $reg_user->lasdID();		
			$key = base64_encode($id);
			$id = $key;
			
			$message = "					
						Hello $uname,
						<br /><br />
						Thank You For Joining ULNA!<br/>
						To complete your registration  please , just click following link<br/>
						<br /><br />
						<a href='http://localhost/ulna/verify.php?id=$id&code=$code'>Click HERE to Activate :)</a>
						<br /><br />
						Thanks,";
						
			$subject = "Confirm Registration";
						
			$reg_user->send_mail($email,$message,$subject);	
			$msg = "
					<div class='alert alert-success'>
						<button class='close' data-dismiss='alert'>&times;</button>
						<strong>Success!</strong>  We've sent an email to $email.
                    Please click on the confirmation link in the email to create your account. 
			  		</div>
					";
		}
		else
		{
			echo "sorry , Query could no execute...";
		}		
	}
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/square/red.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    
     <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url(http://weloveiconfonts.com/api/?family=iconicfill);
@import url(http://fonts.googleapis.com/css?family=Arvo);
/* iconicfill */
[class*="iconicfill-"]:before {
  font-family: 'IconicFill', sans-serif;
}

body,html{width:100%;height:100%;}
body{font-family:'Arvo'; background: #4f3b5b;background: -moz-radial-gradient(center, ellipse cover,  #4f3b5b 0%, #231733 100%);background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#4f3b5b), color-stop(100%,#231733));background: -webkit-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: -o-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: -ms-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: radial-gradient(ellipse at center,  #4f3b5b 0%,#231733 100%);}

.wrap{width:300px;}
.center{
 top:50%;
 transform:translateY(-50%);
 position:relative;
 margin:auto;}

input{
  display:block;
    width: 300px;
    padding: 15px 0 15px 12px;
    font-family: "Arvo";
    font-weight: 400;
    color: #377D6A;
    background: rgba(0,0,0,0.3);
    border: none;
    outline: none;
    color: #fff;
    text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
    border: 1px solid rgba(0,0,0,0.3);
    border-radius: 4px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px  1px rgba(255,255,255,0.2);
    text-indent: 60px;
    transition: all .3s ease-in-out;
    position: relative;
    font-size: 13px;
}
input:focus{
    text-indent: 12px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.4), 0 1px 1px rgba(255,255,255,0.2);
}

label{
    display: block;
    position: static;
    margin: 0;
    padding: 0;
    color:#fff;
    font-family: 'Arvo';
    font-size: 16px;
}

.wrap-label{
  width: 300px;
  height: 34px;
  position: relative;
  padding: 0;
  margin: 0;
  bottom: -15px;
  overflow: hidden;}

.iconicfill-pen-alt2{
    position: absolute;
    left: 10px;
    color: #fff;
    font-size:19px;
    opacity: 1;
    top: 0;
    transform:translateX(-100px);
}

/* ==== Pen animation ==== */
.move-pen{animation: move-pen 1s ease-in infinite alternate;}
@keyframes move-pen{
    from{transform:translateX(-4px) rotate(6deg);}
    to{transform:translateX(4px) rotate(-3deg);}
}
    </style>
</head>
<body> 
      
      
      <div class="hold-transition login-page" id="login"></div>
    <div class="boxLoading"></div>
  <div class="login-logo">
      <a href="index.php"><img src="dist/img/logo3B.png" height="150px" width ="150px"/><b>ULNA</b></a>
  </div>
    

  
    
	<br>
    <form method="post">
    <div class="wrap center"> <!-- Just to center ver and hor -->
         <?php if(isset($msg)) echo $msg;  ?>
  <div class="wrap-label">
     <label for="name">Username</label>
     <p class="iconicfill-pen-alt2"></p>  
  </div>
  <input type="text"  name="txtuname" required  />   
</div>
    
        
        <div class="wrap center"> <!-- Just to center ver and hor -->
  <div class="wrap-label">
     <label for="name">Email</label>
     <p class="iconicfill-pen-alt2"></p>  
  </div>
  <input type="email" name="txtemail" required>
</div> 
        
        
    <div class="wrap center"> <!-- Just to center ver and hor -->
  <div class="wrap-label">
     <label for="name">Password</label>
     <p class="iconicfill-pen-alt2"></p>  
  </div>
 <input type="password"  name="txtpass" required>
</div>
        
        <div class="wrap center"> <!-- Just to center ver and hor -->
  <div class="wrap-label">
     <label for="name">Repeat Password</label>
     <p class="iconicfill-pen-alt2"></p>  
  </div>
 <input type="password"  name="txtpass" required>
</div>

<div class="wrap center"> <!-- Just to center ver and hor -->
  <input type="checkbox" name="remember" required><a href="#"> I agree to the terms</a>
</div> 
        <br>
        <div class="wrap center"> <!-- Just to center ver and hor -->
    <button class="btn btn-danger btn-block btn-flat"  type="submit"  name="btn-signup">Sign Up</button>
</div>
        <div class="wrap center"> <!-- Just to center ver and hor -->
   <a href="index.php" class="text-center">I already have a membership</a>
</div> 
    </form>
  
   
 
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-red',
      radioClass: 'iradio_square-red',
      increaseArea: '20%' // optional
    });
      var $checkedCheckboxes = $('#recurrent_checkin :checkbox[name="remember[]"]:checked'),
    $checkboxes = $('#recurrent_checkin :checkbox[name="remember[]"]');

$checkboxes.click(function() {

if($checkedCheckboxes.length) {
        $checkboxes.removeAttr('required');
    } else {
        $checkboxes.attr('required', 'required');
    }
  });
      });
</script>
</body>
</html>
