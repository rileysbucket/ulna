<?php
session_start();
require_once 'class.user.php';
$user_login = new USER();

if($user_login->is_logged_in()!="")
{
	$user_login->redirect('start.php');
}

if(isset($_POST['btn-login']))
{
	$email = trim($_POST['txtemail']);
	$upass = trim($_POST['txtupass']);
	
	if($user_login->login($email,$upass))
	{
		$user_login->redirect('start.php');
	}
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/red.css">
    <link rel="stylesheet" href="dist/css/loader.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

         <link rel="stylesheet" href="dist/css/video.css">
    
   <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url(http://weloveiconfonts.com/api/?family=iconicfill);
@import url(http://fonts.googleapis.com/css?family=Arvo);
/* iconicfill */
[class*="iconicfill-"]:before {
  font-family: 'IconicFill', sans-serif;
}

body,html{width:100%;height:100%;}
body{font-family:'Arvo'; background: #4f3b5b;background: -moz-radial-gradient(center, ellipse cover,  #4f3b5b 0%, #231733 100%);background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#4f3b5b), color-stop(100%,#231733));background: -webkit-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: -o-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: -ms-radial-gradient(center, ellipse cover,  #4f3b5b 0%,#231733 100%);background: radial-gradient(ellipse at center,  #581830 0%,#231733 100%);}

.wrap{width:300px;}
.center{
 top:50%;
 transform:translateY(-50%);
 position:relative;
 margin:auto;}

input{
  display:block;
    width: 300px;
    padding: 15px 0 15px 12px;
    font-family: "Arvo";
    font-weight: 400;
    color: #377D6A;
    background: rgba(0,0,0,0.3);
    border: none;
    outline: none;
    color: #fff;
    text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
    border: 1px solid rgba(0,0,0,0.3);
    border-radius: 4px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.2), 0 1px  1px rgba(255,255,255,0.2);
    text-indent: 60px;
    transition: all .3s ease-in-out;
    position: relative;
    font-size: 13px;
}
input:focus{
    text-indent: 12px;
    box-shadow: inset 0 -5px 45px rgba(100,100,100,0.4), 0 1px 1px rgba(255,255,255,0.2);
}

label{
    display: block;
    position: static;
    margin: 0;
    padding: 0;
    color:#fff;
    font-family: 'Arvo';
    font-size: 16px;
}

.wrap-label{
  width: 300px;
  height: 34px;
  position: relative;
  padding: 0;
  margin: 0;
  bottom: -15px;
  overflow: hidden;}

.iconicfill-pen-alt2{
    position: absolute;
    left: 10px;
    color: #fff;
    font-size:19px;
    opacity: 1;
    top: 0;
    transform:translateX(-100px);
}

/* ==== Pen animation ==== */
.move-pen{animation: move-pen 1s ease-in infinite alternate;}
@keyframes move-pen{
    from{transform:translateX(-4px) rotate(6deg);}
    to{transform:translateX(4px) rotate(-3deg);}
}
    </style>

</head>
<body>
     <div class="login-logo">
      <a href="index.php"><img src="dist/img/logo3B.png" height="150px" width ="150px"/><b>ULNA</b></a>
  </div>
 

    <div class="row">
        
        
        
        
        
        
        <div class="col-md-6">
            
             
      <div class='main'>
  <div id='media-player' >
    <video id='media-video' >
      <source src='dist/powtoon-g8JLhM1RcdT.mp4' type='video/webm'>
    </video>
  </div>
         
  <div id='media-controls' class='ctrl-box'>
    <button id='play-pause-button' class='play' title='play' onclick='togglePlayPause();'>
      <div class="playfront" id="play-disp">
        <span class="fa fa-play"></span>
      </div>
      <div class="playback" id="user-avi"></div>
    </button>
    <a class="data-name" href="http://ulna.international">Introducing ULNA<div id="play-bar" class="play-bar"></div></a>
    <a class="data-video" href="http://ulna.international">ulna.international</a>
  </div>
</div>
          <!-- general form elements -->
          
          <!-- /.box -->

          <!-- Form Element sizes -->
   
                
                <br>
                <br>
                <br>
                <br>  
<br>
   <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                
                
                <br>
         
          <!-- /.box -->
<br>
   <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                
                
                <br>
          <!-- Input addon -->
          
        </div>
        <!--/.col (left) -->
  
    
        
        
        
        
        
        
        
        
        
        
        
        <div class="col-md-5">
           <div class="hold-transition login-page" id="login"></div>
    <div class="boxLoading"></div>
            <?php 
		if(isset($_GET['inactive']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Sorry!</strong> This Account is not Activated Go to your Email and press on the link we have sent you Activate it. 
			</div>
            <?php
		}
		?>
<form method="post" id="login-form">
        <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Wrong Details!</strong> 
			</div>
            <?php
		}
		?>
    
    
    <br>
     <div class="wrap center"> <!-- Just to center ver and hor -->
  <div class="wrap-label">
     <label for="name">Email</label>
     <p class="iconicfill-pen-alt2"></p>
      
  </div>
  <input type="email"   name="txtemail" required>
        
</div>
     <div class="wrap center"> <!-- Just to center ver and hor -->
        
  <div class="wrap-label">
     <label for="name">Password</label>
     <p class="iconicfill-pen-alt2"></p>
      
  </div>
  <input type="password"  name="txtupass" required>
</div>
    
    <div class="wrap center"> <!-- Just to center ver and hor -->
 <button type="submit" class="btn btn-danger btn-block btn-flat" name="btn-login" id="btn-login">Sign In</button>
</div>
    <br>
     <div class="wrap center"> <!-- Just to center ver and hor -->
  <a href="fpass.php">I forgot my password</a>
         <br>
    <a href="register.php" class="text-center">Register a new membership</a>
</div>
    </form>
        </div>
        
        
        
        
        
        
        
        
    
        
        
        
        
        
        
        
        
   
      </div>
    
    
      
           
    
    
   
          
        <!-- left column -->
        
    
    
    
    
  
 <script src="dist/js/video.js"></script>   
 <script src='http://cdn.jsdelivr.net/vivus/0.2.1/vivus.min.js'></script>           
<!-- /.login-box -->
<script src="dist/js/back.js"></script>
<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery.transit/0.9.9/jquery.transit.min.js'></script>

    <script src="dist/js/write.js"></script>
    
    <!-- Checkbox script -->
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-red',
      radioClass: 'iradio_square-red',
      increaseArea: '20%' // optional
    });
      var $checkedCheckboxes = $('#recurrent_checkin :checkbox[name="remember[]"]:checked'),
    $checkboxes = $('#recurrent_checkin :checkbox[name="remember[]"]');

$checkboxes.click(function() {

if($checkedCheckboxes.length) {
        $checkboxes.removeAttr('required');
    } else {
        $checkboxes.attr('required', 'required');
    }
  });
      });
</script>
    
    	<script>
		// Wait for window load
		$(window).load(function() {
			// Animate loader off screen
			$("#loader").animate({
				top: -200
			}, 1500);
		});
	</script>
</body>
</html>
