<?php
session_start();
require_once 'class.user.php';
$user_home = new USER();

if(!$user_home->is_logged_in())
{
	$user_home->redirect('index.php');
}
$stmt = $user_home->runQuery("SELECT * FROM tbl_users WHERE userID=:uid");
$stmt->execute(array(":uid"=>$_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/green.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
     <!-- <link rel="stylesheet" href="dist/css/licence.css">-->
      <link rel="stylesheet" href="dist/css/credit.css">
    <link rel="stylesheet" href="dist/css/credit.css">
    <link rel="stylesheet" href="dist/css/checkbox.css">
     <link rel="stylesheet" href="dist/css/slideshow.css">
         <link rel="stylesheet" href="dist/css/msgPop.css" />
       <link rel="stylesheet" href="dist/css/ribbon.css" />
              <link rel="stylesheet" href="dist/css/expand.css" />
  

        
<script src="bootstrap/js/bootstrap.min.js"></script>
     <script language="javascript" type="text/javascript" src="dist/js/msgPop.js"></script>
    <script language="javascript" type="text/javascript" src="dist/js/notifs.js"></script>
     <script type="text/javascript" src="dist/js/date_time.js"></script>
    
    
     <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="dist/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>

    
    <script>

    function toggle_dp() {
      dp = $("#datepicker");
      if (dp.attr('datepicker')) {
        dp.datepicker('destroy');
        dp.removeAttr('datepicker');
      } else {
        dp.datepicker();
        dp.attr('datepicker', 1);
      }


    }
  </script>

    <!-- <script type="text/javascript">
	$(document).ready(function(){
		$("#welcome").modal('show');
	});
</script> -->
    <script type="text/javascript">
    $(document).ready(function(){
     var intervalID = 0;
        var time = 10;
        MsgPop.displaySmall = true;
         MsgPop.position = "top-right";
         window.onload = function () {
        MsgPop.open({
    Type:			"success",
    Content:		'Welcome to ULNA <?php echo $row['userName'] ?>!! if you have a licence,select the respective licence category or proceed to make your reservations!<br>This message will close in <span id="time">10</span> seconds!',
    AutoClose:		true,
    BeforeOpen:		function(){
    					time = 10;
    					if(intervalID == 0){
    						intervalID = setInterval(function(){
    							time-=1;
    							document.getElementById('time').innerHTML = time;
    						},1000);
    					}
    				},
    BeforeClose:	function(){
    					time = 10;
    					clearInterval(intervalID);
    					intervalID = 0;
    				},
    CloseTimer:		10000,
    ClickAnyClose:	false,
    HideCloseBtn:	true});
         }
      }); 
    
    
    </script>
    
    
    
    
    
       <script language="javascript" type="text/javascript">
  $(function () {
    $( "#tabs" ).tabs();
  });     
</script>
    
    
     <script language="javascript" type="text/javascript">
        
         var intervalID = 0;
        var time = 10;
        MsgPop.displaySmall = true;
         MsgPop.position = "top-right";
         window.onload = function () {
        MsgPop.open({
    Type:			"success",
    Content:		'Welcome to ULNA <?php echo $row['userName'] ?>!! if you have a licence,select the respective licence category or proceed to make your reservations!<br>This message will close in <span id="time">10</span> seconds!',
    AutoClose:		true,
    BeforeOpen:		function(){
    					time = 10;
    					if(intervalID == 0){
    						intervalID = setInterval(function(){
    							time-=1;
    							document.getElementById('time').innerHTML = time;
    						},1000);
    					}
    				},
    BeforeClose:	function(){
    					time = 10;
    					clearInterval(intervalID);
    					intervalID = 0;
    				},
    CloseTimer:		10000,
    ClickAnyClose:	false,
    HideCloseBtn:	true});
         }
         </script>

    
     
    
<script type="text/javascript">
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    

    
    
    

    
<script type="text/javascript">

$(document).ready(function() {
  $('.lodge_checkbox').click(function() {
      $(".lodge_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.hotel_checkbox').click(function() {
      $(".hotel_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.resturant_checkbox').click(function() {
      $(".resturant_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.flight_checkbox').click(function() {
      $(".flight_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
  <script type="text/javascript">
    
   $(function(){

$('#lodgel').click(function(){
  $('#lodgelicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    
      <script type="text/javascript">
    
   $(function(){

$('#hotell').click(function(){
  $('#hotellicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    <script type="text/javascript">
    
   $(function(){

$('#restl').click(function(){
  $('#resturantlicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
        <script type="text/javascript">
    
   $(function(){

$('#flightl').click(function(){
  $('#flightlicence').modal('show');
  return false;
})

}); 
  </script> 
        
<!--Licence forms--->
   <script type="text/javascript">
    
   $(function(){

$('#lodlicence').click(function(){
  $('#licencecodeL').modal('show');
  return false;
})

}); 
  </script>
    
     <script type="text/javascript">
    
   $(function(){

$('#hotlicence').click(function(){
  $('#licencecodeH').modal('show');
  return false;
})

}); 
  </script> 
    
      <script type="text/javascript">
    
   $(function(){

$('#restlicence').click(function(){
  $('#licencecodeR').modal('show');
  return false;
})

}); 
  </script> 
    
     <script type="text/javascript">
    
   $(function(){

$('#flicence').click(function(){
  $('#licencecodeF').modal('show');
  return false;
})

});          
  </script> 
    
    
    
       
    
  
    
    <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      


ul {
  margin: 0;
  padding: 0;
}
ul li {
  list-style-type: none;
}

p {
  margin: 0;
  padding: 0;
}

.l-block {
  display: block;
}

.l-space {
  margin-bottom: 20px;
}

.flip {
  margin: 0 auto;
}

.creditCard {
  background: #fff;
  border-radius: 10px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.45);
  height: 300px;
  padding: 20px;
  position: relative;
  width: 500px;
}

.creditCard--back {
  background: #415b75;
}

.creditCard--glossy .creditCard-header {
  position: relative;
}
.creditCard--glossy .creditCard-header::before {
  background: #fff;
  content: "";
  display: block;
  height: 30px;
  left: 0;
  opacity: 0.1;
  position: absolute;
  top: 0;
  width: 100%;
}

.creditCard-close {
  background: #10161c;
  border: 2px solid #fff;
  border-radius: 50%;
  color: #fff;
  display: block;
  height: 40px;
  left: -10px;
  line-height: 40px;
  position: absolute;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  top: -10px;
  width: 40px;
}
.creditCard-close:hover {
  background: #22303d;
}

.creditCard-field {
  margin-bottom: 20px;
}
.creditCard-field:last-child {
  margin-bottom: 0;
}

.has-creditCard-field-help {
  position: relative;
}

.creditCard-field-help {
  background: #999;
  border-radius: 50%;
  color: #fff;
  display: block;
  height: 20px;
  line-height: 20px;
  position: absolute;
  right: 5px;
  text-align: center;
  text-decoration: none;
  bottom: 10px;
  width: 20px;
}
.creditCard-field-help:hover {
  background: #636365;
}

.creditCard-header {
  background: #34495e;
  margin: 0 -20px 20px;
  overflow: auto;
  padding: 5px;
}

.creditCard-header--fixed {
  height: 30px;
  margin-bottom: 40px;
}

.creditCard-input {
  border: 1px solid #ccc;
  border-radius: 5px;
  box-sizing: border-box;
  font: 16px sans-serif;
  padding: 10px;
  width: 100%;
}

.creditCard-input--fixed {
  background: #fff;
  border: 0;
  border-radius: 0;
  font-weight: bold;
  height: 36px;
  position: relative;
  text-align: right;
}

.creditCard-highlight {
  background: rgba(255, 0, 0, 0.3);
  border: 5px solid red;
  border-radius: 50%;
  padding: 10px 25px;
  position: absolute;
  right: -5px;
  top: 4px;
}

.creditCard-label {
  display: block;
  font-size: 12px;
  font-weight: bold;
  margin-bottom: 5px;
  text-transform: uppercase;
}

.creditCard-select {
  margin-top: 10px;
}

.creditCard-title {
  color: #fff;
  font-size: 20px;
  font-weight: normal;
  margin: 13px 0 0;
  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.75);
}

.creditCard-types {
  margin-top: 5px;
  text-align: right;
}

.creditCard-type {
  display: inline;
  margin-right: 5px;
  -webkit-filter: drop-shadow(0 1px 2px rgba(0, 0, 0, 0.45));
}
.creditCard-type:last-child {
  margin-right: 0;
}

.flip {
  -moz-perspective: 500px;
  -webkit-perspective: 500px;
  perspective: 500px;
  height: 300px;
  width: 500px;
}

.is-flipped .flip-inner {
  -moz-transform: rotateY(180deg);
  -ms-transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
  transform: rotateY(180deg);
}

.flip-inner {
  height: 100%;
  position: relative;
  -webkit-transition: 0.6s;
  transition: 0.6s;
  -webkit-transform-style: preserve-3d;
  -moz-transform-style: preserve-3d;
  transform-style: preserve-3d;
}

.flip-content {
  -moz-backface-visibility: hidden;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
}

.flip-content--front {
  z-index: 2;
}

.flip-content--back {
  -moz-transform: rotateY(180deg);
  -ms-transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
  transform: rotateY(180deg);
}

    </style>
    
      
 
    <script src="tabs.js"></script>
      
   <style>
/* Paste this css to your style sheet file or under head tag */
/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(dist/img/loader-64x/Preloader_2.gif) center no-repeat #fff;
}
</style>
    <script>
	//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
</script>
</head>
<body class="viewdetail"> 
    <div class="se-pre-con"></div>
   
        <div id="tabs">
<div id="lodgelicence" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-warning">
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"  data-toggle="tooltip" title="Close">
                  <span aria-hidden="true" >&times;</span></button>
                
                <h4 class="modal-title">Complete the form below to get your<b>|Lodge|</b>licence</h4>
              </div>

            </div>
            
            <div class="modal-body">
               <div class="tabbable" >
                     <ul class="nav nav-tabs">
 <li class="active"><a  href="#tab1l" data-toggle="tab">Get|Licence</a></li>
               
                          
        </ul>
                </div>
            <div class="modal-body">
                     <div class="tab-content">
   <!--Profile Form-------------------------------------------------------------->                      
        <div class="tab-pane active" id="tab1l">
        	 <form action="lodges/lodgelicence.php" method="post">
               
                
                  <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Company Name Already Exists.Try a New Name or an abbreviation of your company.</strong> 
			</div>
            <?php
		}
		?> 
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="company" name="cname" required>
                    </div>
                <div class="form-group">
                        <input type="text" class="form-control" value="Lodge " readonly="readonly" name="sub_type" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="representative" name="crep" required>
                    </div>
 
<button type="submit" id="getlodgelicence" name="getlodgelicence" class="btn btn-primary"data-toggle="tooltip" title="Submit" >Submit</button>
</form> 
        </div>
                         <!--Profile Form End-------------------------------------------------------------->             
            
                         
                   

        </div>
         
         
        <div class="modal-footer">
    <div class="divright">
        
       
      <p class="user">Powered by ULNA</p><img  style=height:70px src="dist/img/logo3B.png"/>
    </div>
   </div>

</div>
        </div>
                    </div>
    </div>
 
</div>
   
 
 <div class="Clearboth"></div>
 

 
    
    
    
         <div id="tabs">
     
<div id="hotellicence" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-warning">
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complete the form below to get your<b>|Hotel|</b>licence</h4>
              </div>
            
            <div class="modal-body">
               <div class="tabbable" >
                     <ul class="nav nav-tabs">
 <li class="active"><a  href="#tab1l" data-toggle="tab">Get|Licence</a></li>                   
        </ul>
                </div>
            <div class="modal-body">
                     <div class="tab-content">
   <!--Profile Form-------------------------------------------------------------->                      
        <div class="tab-pane active" id="tab1l">
        	 <form action="hotels/hotellicence.php" method="post">
                <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Details Saved Successfully.Proceed to next section</strong> 
			</div>
            <?php
		}
		?>
                
                  <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Company Name Already Exists.Try a New Name or an abbreviation of your company.</strong> 
			</div>
            <?php
		}
		?> 
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="company" name="cname" required>
                    </div>
                <div class="form-group">
                        <input type="text" class="form-control" value="Hotel " readonly="readonly" name="sub_type" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="representative" name="crep" required>
                    </div>
 
<button type="submit" class="btn btn-primary" data-toggle="tooltip" title="Submit" >Submit</button>
</form> 
        </div>                       
        </div>
         
          
        
        <div class="modal-footer">
    <div class="divright">
        
       
      <p class="user">Powered by ULNA</p><img  style=height:70px src="dist/img/logo3B.png"/>
    </div>
   </div>

</div>
        </div>
                    </div>
    </div>
 
</div>
    </div>
 
 <div class="Clearboth"></div>
 

<div id="tabs">
     
<div id="resturantlicence" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-warning">
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complete the form below to get your<b>|Resturant|</b>licence</h4>
              </div>
            
            <div class="modal-body">
               <div class="tabbable" >
                     <ul class="nav nav-tabs">
 <li class="active"><a  href="#tab1r" data-toggle="tab">Get|Licence</a></li>                   
        </ul>
                </div>
            <div class="modal-body">
                     <div class="tab-content">
   <!--Profile Form-------------------------------------------------------------->                      
        <div class="tab-pane active" id="tab1r">
        	 <form action="resturants/resturantlicence.php" method="post">
                <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Details Saved Successfully.Proceed to next section</strong> 
			</div>
            <?php
		}
		?>
                
                  <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Company Name Already Exists.Try a New Name or an abbreviation of your company.</strong> 
			</div>
            <?php
		}
		?> 
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="company" name="cname" required>
                    </div>
                <div class="form-group">
                        <input type="text" class="form-control" value="Resturant " readonly="readonly" name="sub_type" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="representative" name="crep" required>
                    </div>
 
<button type="submit" class="btn btn-primary" data-toggle="tooltip" title="Submit">Submit</button>
</form> 
        </div>                       
        </div>
         
          
        
        <div class="modal-footer">
    <div class="divright">
        
       
      <p class="user">Powered by ULNA</p><img  style=height:70px src="dist/img/logo3B.png"/>
    </div>
   </div>

</div>
        </div>
                    </div>
    </div>
 
</div>
    </div>




    <div id="tabs">
     
<div id="flightlicence" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-warning">
        <div class="modal-content">
            <div class="modal-header primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Complete the form below to get your<b>|Flight|</b>licence</h4>
              </div>
            
            <div class="modal-body">
               <div class="tabbable" >
                     <ul class="nav nav-tabs">
 <li class="active"><a  href="#tab1f" data-toggle="tab">Get|Licence</a></li>                   
        </ul>
                </div>
            <div class="modal-body">
                     <div class="tab-content">
   <!--Profile Form-------------------------------------------------------------->                      
        <div class="tab-pane active" id="tab1r">
        	 <form action="flights/flightlicence.php" method="post">
                <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Details Saved Successfully.Proceed to next section</strong> 
			</div>
            <?php
		}
		?>
                
                  <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Company Name Already Exists.Try a New Name or an abbreviation of your company.</strong> 
			</div>
            <?php
		}
		?> 
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="company" name="cname" required>
                    </div>
                <div class="form-group">
                        <input type="text" class="form-control" value="Airport " readonly="readonly" name="sub_type" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="representative" name="crep" required>
                    </div>
 
<button type="submit" class="btn btn-primary"data-toggle="tooltip" title="Submit" >Submit</button>
</form> 
        </div>                       
        </div>
         
          
        
        <div class="modal-footer">
    <div class="divright">
        
       
      <p class="user">Powered by ULNA</p><img  style=height:70px src="dist/img/logo3B.png"/>
    </div>
   </div>

</div>
        </div>
                    </div>
    </div>
 
</div>
    </div>
    
    
    
    





<!--Licence Modals-------------------------------------------------------------------------------------------------------->
<!--Lodge Modal Licence------>


<div id="licencecodeL" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-danger">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Enter Your Lodge Licence</h4>
            </div>
            
            <div class="modal-body">               
<form action="lodges/lodgelicenceverify.php" method="post">
       
                
                  <?php
        if(isset($_GET['error']))
		{
			?>
                            <script type="text/javascript">  
MsgPop.closeAll(); // Removes messages and resets MsgPop object
MsgPop.displaySmall = false; // Global switch that makes messages full screen

MsgPop.open({
Type:"error",
Content:"Incrorrect Company Name or licence code.If you do not have a licence,please purchase one",
AfterClose:function(){
MsgPop.displaySmall = false;
}
});
</script>
 
            <?php
		}
		?>
                
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="Company Name" name="cname" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="Licence Code" name="token" required>
                    </div>
 
<button type="submit" name="golodge" id="golodge" class="btn btn-success" >Submit</button>
</form> 

        </div>
        
    </div>
              <div class="modal-footer">
                  <p>Powered by ULNA</p><img  style=height:40px src="dist/img/logo3B.png"/>
   </div>
</div>
</div>

<!--Hotel Modal Licence------>
<div id="licencecodeH" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-danger">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Enter Your Hotel Licence</h4>
            </div>
            
            <div class="modal-body">               
<form action="hotels/hotellicenceverify.php" method="post">
       
                
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Company Name" name="cname" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="Licence Code" name="token" required>
                    </div>
 
<button type="submit" class="btn btn-success" name="gohotel" id="gohotel" data-toggle="tooltip" title="Submit" >Submit</button>
</form> 

        </div>
        
    </div>
               <div class="modal-footer">
                  <p>Powered by ULNA</p><img  style=height:40px src="dist/img/logo3B.png"/>
   </div>
</div>
</div>


<!--Resturant licence modal---->

<div id="licencecodeR" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-danger">
        <div class="modal-content">
            <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Enter Your Resturant Licence</h4>
            </div>
            
            <div class="modal-body">               
<form action="resturants/resturantlicenceverify.php" method="post">
       
                
                
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="Company Name" name="cname" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="Licence Code" name="token" required>
                    </div>
 
<button type="submit" class="btn btn-success" name="goresturant" id="goresturant" data-toggle="tooltip" title="Submit" >Submit</button>
</form> 

        </div>
        
    </div>
               <div class="modal-footer">
                  <p>Powered by ULNA</p><img  style=height:40px src="dist/img/logo3B.png"/>
   </div>
</div>
</div>

<!--Flights licence modal---->

<div id="licencecodeF" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-danger">
        <div class="modal-content">
            <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" title="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Enter Your Flights Licence</h4>
            </div>
            
            <div class="modal-body">               
<form action="flights/flightlicenceverify.php" method="post">
       
                
                <div class="form-group">
                        <input type="text" class="form-control" placeholder="Company Name" name="cname" required>
                    </div>
                 <div class="form-group">
                        <input type="text" class="form-control" placeholder="Licence Code" name="token" required>
                    </div>
 
<button type="submit" class="btn btn-success" name="goflight" id="goflight" data-toggle="tooltip" title="Submit" >Submit</button>
</form> 

        </div>
        
    </div>
             <div class="modal-footer">
                  <p>Powered by ULNA</p><img  style=height:40px src="dist/img/logo3B.png"/>
   </div>
</div>
</div>
  
<div class="hold-transition skin-red sidebar-mini">       
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
  
      <span class="logo-mini"><b>UL</b>NA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ULNA</b></span>
         
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
         <!-- <img src="dist/img/logo3B.png" width="64px" height="64px" class="brand-logo center"/> --> 
      </a>
        
        

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="userfiles/avatars/default.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo $row['userName'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="userfiles/avatars/default.jpg"class="img-circle" alt="User Image">

                <p>
                   <?php echo $row['userName'] ?>
                  <small>Member since:<?php echo $row['join_date'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <?php echo $row['userName'] ?>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> <br><br>
          
        </div>
      </div>
      <!--time-->
              <div class="box-success direct-chat-success">
                <div class="direct-chat-msg right">
                  <div class="direct-chat-text">
                   <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
                  </div>
                </div>
          </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><span>Advertise Here</span></li>
        <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="dist/img/photo1.png" alt="First slide">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo2.png" alt="Second slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo3.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
           <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="dist/img/photo3.jpg" alt="First slide">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo3.jpg" alt="Second slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="dist/img/photo4.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
          <li class="active"><a href="index.php"><i class="fa fa-question-circle"></i>Help</a></li>
          <li class="active"><a href="index.php"><i class="fa fa-question-circle"></i>Contact Us</a></li>
      </ul>
    </section>
  </aside>
    
<div class="example-modal" id="memberModal">
        <div class="modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
 
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Lodges</h3>
              <p>Lodge Management</p>
            </div>
            <div class="icon">
              <i class="fa fa-bed"></i>
            </div>
            <a href="" class="small-box-footer" >Manage <i class="fa fa-cubes" id="lodlicence" data-toggle="tooltip" title="Manage"></i></a>
                <div class="tooltip">Facebook</div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            
          <div class="small-box bg-purple">
            <div class="inner">
                
              <h3>Hotels<sup style="font-size: 20px"></sup></h3>

              <p>Hotel Management</p>
            </div>
            <div class="icon">
              <i class="fa fa-building"></i>
            </div>
            <div class="ribbon-wrapper"><div class="glow">&nbsp;</div>
		<div class="ribbon-front">
			Coming Soon!
          
		</div>
		<div class="ribbon-edge-topleft"></div>
		<div class="ribbon-edge-topright"></div>
		<div class="ribbon-edge-bottomleft"></div>
		<div class="ribbon-edge-bottomright"></div>
	</div>
          </div>
            
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Resturants</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="fa fa-cutlery"></i>
            </div>
          <div class="ribbon-wrapper"><div class="glow">&nbsp;</div>
		<div class="ribbon-front">
			Coming Soon!
          
		</div>
		<div class="ribbon-edge-topleft"></div>
		<div class="ribbon-edge-topright"></div>
		<div class="ribbon-edge-bottomleft"></div>
		<div class="ribbon-edge-bottomright"></div>
	</div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>Flights</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="fa fa-plane"></i>
            </div>
            <div class="ribbon-wrapper"><div class="glow">&nbsp;</div>
		<div class="ribbon-front">
			Coming Soon!
          
		</div>
		<div class="ribbon-edge-topleft"></div>
		<div class="ribbon-edge-topright"></div>
		<div class="ribbon-edge-bottomleft"></div>
		<div class="ribbon-edge-bottomright"></div>
	</div>
          </div>
        </div>
          
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence</span>
              <span class="info-box-number">0%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                  Get Licence  <a href=""><i class="fa fa-shopping-bag" id="lodgel" data-toggle="tooltip" title="Get your Lodge licence here"  ></i></a>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-purple">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence</span>
              <span class="info-box-number">100%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                <span class="progress-description">
                   Get Licence   <a href=""><i class="fa fa-shopping-bag" id="hotell" data-toggle="tooltip" title="Hotel licence will be available soon"  ></i></a>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence</span>
              <span class="info-box-number">100%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                 <span class="progress-description">
                  Get Licence   <a href=""><i class="fa fa-shopping-bag" id="restl" data-toggle="tooltip" title="Resturant licence will be available soon"  ></i></a>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
           <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-maroon">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence</span>
              <span class="info-box-number">100%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                   <span class="progress-description">
                  Get Licence   <a href=""><i class="fa fa-shopping-bag" id="flightl"  data-toggle="tooltip" title="flight licence will be available soon" ></i></a>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
      </div>
        
        
          <div id="lodge_results"></div>
        
        
          <section id="fetch">
         <form class="sidebar-form">
        <div class="input-group">
          <input type="text"  class="form-control" placeholder="Search for a lodge,hotel,resturant or flights ..." id="search_term">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
        </section>
        

        
        
        

    
     
<style>
    
 

.modalDialog {
	position: fixed;
	font-family: Arial, Helvetica, sans-serif;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: rgba(0,0,0,0.8);
	z-index: 99999;
	opacity:0;
	-webkit-transition: opacity 400ms ease-in;
	-moz-transition: opacity 400ms ease-in;
	transition: opacity 400ms ease-in;
	pointer-events: none;
}

   
    

.modalDialog:target {
	opacity:1;
	pointer-events: auto;
}

.modalDialog > div {
	width: 400px;
	position: relative;
	margin: 10% auto;
	padding: 5px 20px 13px 20px;
	border-radius: 10px;
	background: #fff;
	background: -moz-linear-gradient(#fff, #999);
	background: -webkit-linear-gradient(#fff, #999);
	background: -o-linear-gradient(#fff, #999);
}



.close {
	background: #606061;
	color: #FFFFFF;
	line-height: 25px;
	position: absolute;
	right: -12px;
	text-align: center;
	top: -10px;
	width: 24px;
	text-decoration: none;
	font-weight: bold;
	-webkit-border-radius: 12px;
	-moz-border-radius: 12px;
	border-radius: 12px;
	-moz-box-shadow: 1px 1px 3px #000;
	-webkit-box-shadow: 1px 1px 3px #000;
	box-shadow: 1px 1px 3px #000;
}

.close:hover { background: #00d9ff; }


        
        </style>
           
  


          
<!--Lodges-->
        
              <div class="page-wrap">
  <div id="lodge-container"></div>
</div>
        
        

 <!--<img src="lodges/images/logos/{{photo}}">-->
             
   <script id="lodgeTemplate" type="text/x-handlebars-template">


  
   
{{#each lodges}}

<div class="character-details">
<h2>{{cname}}  </h2>
</div>
 <div class="box box-primary collapsed-box">
            <div class="box-header">
              
               <div class="box box-widget widget-user">
               <div class="widget-user-header bg-white" style="background: url('lodges/images/imagez/{{headerimg}}'),url('userfiles/placeholder/imgholder.png')">
               <h3 class="widget-user-username"><b>{{cname}}</b></h3>
               <h5 class="widget-user-desc">({{sub_type}})</h5>
               </div>
               

               <div class="widget-user-image">
      <img class="img-circle" class="img-responsive" src="lodges/images/logos/{{logos}}" 
      alt="No Image Provided" onerror="this.src='userfiles/placeholder/imgholder.png'">
            </div>
            
            
             <br>
           
               
               <div class="col-sm-4 border-right"> 
                <div class="description-block">
                    <div class="comment-text"><strong><i class="fa fa-map-marker margin-r-5"></i></strong>Location</div>
                    <span class="description-text">{{company_location}}<h5 class="widget-user-desc">
                  </div>
                 </div>
                 
                 
                
                 
                  <div class="col-sm-4 border-right"> 
                          <div class="description-block">
                    <div class="comment-text"><strong><i class="fa fa-file margin-r-5" type="button" ></i></strong>
                    
                    
                    
                    
                    About</div
                    <span class="description-text">{{company_shortbio}}
                    
                    <p>    </p></span>
                  </div>
                  </div>
                  
                  
                  
                   <div class="col-sm-4">
                  <div class="description-block">
                  
                 
                  
                  
                  
                  
                    <a href="#" class="btn btn-primary btn-xs " onclick="toggle_dp();"><b>Book Now</b></a>
                    
                  </div>
                   </div>
                     
                 </div>
              
              <div class="box-tools pull-left">  
                <button type="button" class="btn-floating btn-small waves-effect waves-light red" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
          
              
              
              
              
              
               <div class="col-md-6">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="lodges/images/logos/{{logos}}" alt="No Image onerror="this.src='userfiles/placeholder/imgholder.png'">
                <span class="username"><a href="#">{{cname}}</a></span>
                <span class="description">Shared publicly - 7:30 PM Today</span>
              </div>
            </div>
            
            
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive pad" src="lodges/images/imagez/{{headerimg}}"alt="Photo" onerror="this.src='userfiles/placeholder/imgholder.png'">
              <p>{{company_shortbio}}</p>
              <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-book"></i> Make Reservation</button>
              <button type="button" class="btn btn-success btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button>
              <span class="pull-right text-muted">127 likes</span>
            </div>
            
            <!-- /.box-body -->
            <div class="box-footer box-comments">
              <div class="box-comment">
                <!-- User image -->
                

                <div class="comment-text">
                      <span class="username">
                        About
                      </span><!-- /.username -->
                  <p>{{company_longbio}}</p>
                </div>
                <!-- /.comment-text -->
              </div>
              <!-- /.box-comment -->
              <div class="box-comment">
                <!-- User image -->
                

               <div class="comment-text">
                      <span class="username">
                        Services
                      </span><!-- /.username -->
                   <p>
                <span class="label label-danger">{{service1}}</span>
                <span class="label label-success">{{service2}}</span>
                <span class="label label-info">{{service3}}</span>
                <span class="label label-warning">{{service4}}</span>
                <span class="label label-primary">{{service5}}</span>
              </p>

                </div>
                <!-- /.comment-text -->
              </div>
              <!-- /.box-comment -->
            </div>
            <!-- /.box-footer -->
            <div class="box-footer">
             <span class="username">
                        Location:
                      </span><!-- /.username -->
                  {{cname}}
             
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
        
        
        
        <div class="col-md-6">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="lodges/images/logos/{{logos}}"  alt="No Image"onerror="this.src='userfiles/placeholder/imgholder.png'">
                <span class="username"><a href="#">{{cname}} Gallery</a></span>
              </div>
            </div>
            
            
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive pad" src="lodges/images/imagez/{{headerimg}}"  alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
            </div>
            <!-- /.box-body -->
            
            
            <!-- /.box-footer -->
            <div class="box-footer">
             <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="lodges/images/logos/{{logos}}"   alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
                       
                          <span class="description">{{cname}}</span>
                  </div>
                  <!-- /.user-block -->
                  <div class="row margin-bottom">
                    <div class="col-sm-6">
                      <img class="img-responsive" src="lodges/images/imagez/{{imageA}}"  alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-6">
                          <img class="img-responsive" src="lodges/images/imagez/{{imageA}}"  alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
                          <br>
                          <img class="img-responsive" src="lodges/images/imagez/{{imageB}}"  alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                          <img class="img-responsive" src="lodges/images/imagez/{{imageC}}" alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
                          <br>
                          <img class="img-responsive round" src="lodges/images/imagez/{{imageD}}" alt="No Image" onerror="this.src='userfiles/placeholder/imgholder.png'">
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <ul class="list-inline">
                    <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>
                    </li>
                 </ul>
                </div>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
           
              
              
              <!--Body inner end-->
              
              
            </div>
            <!-- /.box-body -->
          </div>



       

     {{/each}}

</script>
        
  <script src="dist/handlebars/dist/handlebars.js"></script>
    <script src="request.js"></script>
      </section>
    </div>
       </div>
    
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2015-2017 <a href="http://iteslink.com">ULNA</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i href="logout.php" class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
    </div>
    
    
<!-- ./wrapper -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- jQuery 2.2.3 -->
  <script src="plugins/morris/morris.js"></script>  
  <script src="plugins/morris/morris.min.js"></script> 

    
    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script> 
 <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
  <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>  
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jQueryUI/jquery-ui.js"></script>
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jQueryUI/jquery-ui.js"></script>
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
    
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!--<script src="dist/js/licence.js"></script>-->
    <script src="dist/js/credit.js"></script>
    <script src="dist/js/expand.js"></script>

   
    
</body>
</html>
