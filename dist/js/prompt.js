$(function() {
  
  // Standard alert
  $('.alert').on('click', function() {
    $.alertable.alert('Howdy!').always(function() {
      console.log('Alert dismissed');
    });
  });
  
  // Standard confirm
  $('.confirm').on('click', function() {
    $.alertable.confirm('You sure?').then(function() {
      console.log('Confirmation submitted');
    }, function() {
      console.log('Confirmation canceled');      
    });
  });
  
  // Standard prompt
  $('.prompt').on('click', function() {
    $.alertable.prompt('How many?').then(function(data) {
      console.log('Prompt submitted', data);
    }, function() {
      console.log('Prompt canceled');      
    });
  });

  // Standard alert with custom show/hide functions (uses velocity.js)
  $('.alert-vel').on('click', function() {
    $.alertable.alert('Howdy!', {
      show: function() {
        $(this.overlay).velocity('transition.fadeIn', 300);        
        $(this.modal).velocity('transition.shrinkIn', 300);
      },
      hide: function() {
        $(this.overlay).velocity('transition.fadeOut', 300);
        $(this.modal).velocity('transition.shrinkOut', 300);
      } 
    });
  });
  
  // Prompt (login)
  $('.prompt-login').on('click', function() {
    $.alertable.prompt('Login to continue', {
      prompt:
      '<input type="text" class="alertable-input" name="username" placeholder="Username">' +
      '<input type="password" class="alertable-input" name="password" placeholder="Password">' +
      '<label>' +
      '<input type="checkbox" name="remember" value="true"> ' +
      'Remember me' +
      '</label>'
    }).then(function(data) {
      console.log('Login submitted', data);
    }, function() {
      console.log('Login canceled');
    });
  });

});