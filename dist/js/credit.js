(function() {
  this.CCForm = (function() {
    function CCForm(options) {
      this.options = options;
      this.init();
    }

    CCForm.prototype.init = function() {
      return this.events();
    };

    CCForm.prototype.events = function() {
      return this.options.toggle.on('click', (function(_this) {
        return function(e) {
          e.preventDefault();
          return _this.flipCard();
        };
      })(this));
    };

    CCForm.prototype.flipCard = function() {
      return this.options.container.toggleClass(this.options.activeClass);
    };

    return CCForm;

  })();

  $(function() {
    var ccf;
    return ccf = new CCForm({
      toggle: $('.flip-toggle'),
      container: $('.flip'),
      activeClass: 'is-flipped'
    });
  });

}).call(this);