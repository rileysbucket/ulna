<?php
 ob_start();
 session_start();
 require_once 'dbconfig.php';
 
 // it will never let you open index(login) page if session is set
 if ( isset($_SESSION['lodge'])!="" ) {
  header("Location: home.php");
  exit;
 }
 
 $error = false;
 
 if( isset($_POST['submit-lodge']) ) { 
  
  // prevent sql injections/ clear user invalid inputs
     $RandomString = rand(0,1000000000);
  $cname = trim($_POST['cname']);
$crep = trim($_POST['crep']);
$subtype = trim($_POST['subtype']);
  $token = strip_tags('token');
  
  // prevent sql injections / clear user invalid inputs
  
  if(empty($cname)){
   $error = true;
   $cnameError = "Please enter your email address.";
  } else if ( !filter_var($cname,FILTER_VALIDATE_CNAME) ) {
   $error = true;
   $cnameError = "Please enter valid company name.";
  }
  
  if(empty($token)){
   $error = true;
   $tokenError = "Please enter your token.";
  }
  
  // if there's no error, continue to login
  if (!$error) {
   
   $token = hash('sha256', $token); // password hashing using SHA256
  
   $res=mysql_query("SELECT  cname,subtype,token, FROM lodges WHERE cname='$cname'");
   $row=mysql_fetch_array($res);
   $count = mysql_num_rows($res); // if uname/pass correct it returns must be 1 row
   
   if( $count == 1 && $row['cname']==$cname) {
    $_SESSION['lodge'] = $row['cname'];
    header("Location: home.php");
   } else {
    $errMSG = "Incorrect Credentials, Try again...";
   }
    
  }
  
 }
?>