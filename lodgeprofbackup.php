<?php

session_start();

$con=mysqli_connect("localhost","root","Genious2016","ulna");

if (!isset($_SESSION['lodge'])) {
    header("Location:lodge.php");
}
$res = mysqli_query($con, "SELECT * FROM lodges WHERE lodge_id=".$_SESSION['lodge']); 
$row = mysqli_fetch_array($res);
?>

<?php

	require_once '../con.php';
	
	if(isset($_GET['delete_id']))
	{
		// select image from db to delete
		$stmt_select = $DB_con->prepare('SELECT logos FROM ulnapics WHERE picID =:uid');
		$stmt_select->execute(array(':uid'=>$_GET['delete_id']));
		$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
		unlink("user_images/".$imgRow['logos']);
		
		// it will delete an actual record from db
		$stmt_delete = $DB_con->prepare('DELETE FROM ulnapics WHERE picID =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_id']);
		$stmt_delete->execute();
		
		header("Location:../lodgeprofile.php");
	}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ULNA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/flat/green.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <link rel="stylesheet" href="../dist/css/licence.css">
      <link rel="stylesheet" href="../dist/css/credit.css">
    <link rel="stylesheet" href="../dist/css/credit.css">
    <link rel="stylesheet" href="../dist/css/checkbox.css">
     <link rel="stylesheet" href="../dist/css/slideshow.css">
         <link rel="stylesheet" href="../dist/css/msgPop.css" />
        
<script src="../bootstrap/js/bootstrap.min.js"></script>
     <script language="javascript" type="text/javascript" src="../dist/js/msgPop.js"></script>
    <script language="javascript" type="text/javascript" src="../dist/js/notifs.js"></script>
     <script type="text/javascript" src="../dist/js/date_time.js"></script>
    

    <!-- <script type="text/javascript">
	$(document).ready(function(){
		$("#welcome").modal('show');
	});
</script> -->
    
    
    
    
    
    
       <script language="javascript" type="text/javascript">
  $(function () {
    $( "#tabs" ).tabs();
  });     
</script>
    
<script type="text/javascript">
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    

    
    
    
     <script type="text/javascript">
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
 </script>
    
<script type="text/javascript">

$(document).ready(function() {
  $('.lodge_checkbox').click(function() {
      $(".lodge_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.hotel_checkbox').click(function() {
      $(".hotel_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.resturant_checkbox').click(function() {
      $(".resturant_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
    <script type="text/javascript">

$(document).ready(function() {
  $('.flight_checkbox').click(function() {
      $(".flight_checkbox").not(this).attr('checked', false);
  });
});
    </script>
    
  <script type="text/javascript">
    
   $(function(){

$('#lodgel').click(function(){
  $('#lodgelicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    
      <script type="text/javascript">
    
   $(function(){

$('#hotell').click(function(){
  $('#hotellicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
    <script type="text/javascript">
    
   $(function(){

$('#restl').click(function(){
  $('#resturantlicence').modal('show');
  return false;
})

}); 
  </script> 
    
    
        <script type="text/javascript">
    
   $(function(){

$('#flightl').click(function(){
  $('#flightlicence').modal('show');
  return false;
})

}); 
  </script> 
        
<!--Licence forms--->
   <script type="text/javascript">
    
   $(function(){

$('#lodlicence').click(function(){
  $('#licencecodeL').modal('show');
  return false;
})

}); 
  </script>
    
     <script type="text/javascript">
    
   $(function(){

$('#hotlicence').click(function(){
  $('#licencecodeH').modal('show');
  return false;
})

}); 
  </script> 
    
      <script type="text/javascript">
    
   $(function(){

$('#restlicence').click(function(){
  $('#licencecodeR').modal('show');
  return false;
})

}); 
  </script> 
    
     <script type="text/javascript">
    
   $(function(){

$('#flicence').click(function(){
  $('#licencecodeF').modal('show');
  return false;
})

}); 
  </script> 
 
    <script src="../../tabs.js"></script>
        
</head>
<body> 
<div class="hold-transition skin-red sidebar-mini">       
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="lodgeprofile.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UL</b>NA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ULNA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../userfiles/avatars/default.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo $row['cname'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../userfiles/avatars/default.jpg"class="img-circle" alt="User Image">

                <p>
                   <?php echo $row['cname'] ?>
                  <small>Licence Registered on: <?php echo $row['created_on'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../userfiles/avatars/default.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <?php echo $row['cname'] ?>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> <br><br>
          
        </div>
      </div>
      <!--time-->
              <div class="box-success direct-chat-success">
                <div class="direct-chat-msg right">
                  <div class="direct-chat-text">
                   <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
                  </div>
                </div>
          </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><span>Advertise Here</span></li>
        <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="../dist/img/photo1.png" alt="First slide">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo2.png" alt="Second slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo3.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
           <li>
          <div class="box box-solid">
            <!-- /.box-header -->
            
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
               <!-- <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>-->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="../dist/img/photo3.jpg" alt="First slide">

                    <div class="carousel-caption">
                     Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo3.jpg" alt="Second slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                  <div class="item">
                    <img src="../dist/img/photo4.jpg" alt="Third slide">

                    <div class="carousel-caption">
                      Advertise Here
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </li>
          <li class="active"><a href="index.php"><i class="fa fa-question-circle"></i>Help</a></li>
          <li class="active"><a href="index.php"><i class="fa fa-question-circle"></i>Contact Us</a></li>
      </ul>
    </section>
  </aside>
    
    
    
    

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
 
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <!-- ./col -->
            <ol class="breadcrumb">
        <li><a href="active">Licence</a></li>
        <li class="active">Profile Preview</li>
      </ol>
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-certificate"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Licence Status</span>
              <span class="info-box-number">100%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 0%"></div>
              </div>
                  <span class="progress-description">
                  Ref:<?php echo $row['token'] ?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
         
          <div class="col-md-4">
          <div class="box box-widget widget-user">
          <div class="widget-user-header bg-black" style="background: url('../dist/img/photo1.png') center center;">
              <h3 class="widget-user-username"><b><?php echo $row['cname'] ?></b></h3>
              <h5 class="widget-user-desc"><?php echo $row['sub_type'] ?></h5>
            </div>
 <div class="widget-user-image">
              <img class="img-circle" src="../userfiles/avatars/default.jpg" alt="User Avatar">
            </div>
           <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                    
                  <div class="description-block">
                    <h5 class="description-header"><strong><i class="fa fa-map-marker margin-r-5"></i></strong>Location</h5>
                    <span class="description-text"><h5 class="widget-user-desc">
                        <?php if (empty($row['company_location'])) {
                        echo "Not Provided";} 
                            else {
                        echo $row['company_location'];}?></h5></span>
                  </div>
                 
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><strong><i class="fa fa-file margin-r-5"></i></strong>About</h5>
                    <span class="description-text"><?php if (empty($row['company_shortbio'])) {
                        echo "Not Provided";} 
                            else {
                        echo $row['company_shortbio'];}?></h5></span></span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <a href="#" class="btn btn-primary btn-xs"><b>Book Now</b></a>
                      <a href="lodgedetail.php" class="btn btn-success btn-xs">View</a>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
          
          
      </div>
      
       
      
        
         
      
  
        <ol class="breadcrumb">
        <li><a href="active">Profile Management</a></li>
        <li class="active">Timeline</li>
      </ol>
        <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
                 
                       
              <img class="profile-user-img img-responsive img-circle" src="../userfiles/avatars/default.jpg" alt="User profile picture">
                <h3 class="profile-username text-center"> <?php echo $row['cname'] ?></h3>
                <p class="text-muted text-center"> <?php echo $row['sub_type'] ?></p>
 <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Check your Internet Connection or Contact Support for Assistance</strong> 
			</div>
            <?php
		}
		?> 
                
                 <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Profile saved successfully.</strong> 
			</div>
            <?php
		}
		?> 
              <div class="box-body">
                 <form action="saveprofile.php" method="post">
              <strong><i class="fa fa-book margin-r-5"></i> Short Bio</strong>

              <p class="text-muted">
                  <input type="text" class="form-control"name="company_shortbio" id="company_shortbio">
              </p>
              <hr>  
                   <strong><i class="fa fa-book margin-r-5"></i> long Bio</strong>
                  <textarea class="form-control" type="text" placeholder="Long Bio" name="company_longbio" id="company_longbio"></textarea>
              </p>
                  
                    <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                  <input type="text" class="form-control" name="company_location" id="company_location" >

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Services</strong>
<!-- Services----------------------------------------------------------------------------->
              <p>
                <span class="label label-danger"><input type="text" class="form-control"  name="service1" id="service1"></span>
                <span class="label label-info"><input type="text" class="form-control"  name="service2" id="service2"></span>
                <span class="label label-warning"><input type="text" class="form-control"  name="service3" id="service3"></span>
                <span class="label label-primary"><input type="text" class="form-control"  name="service4" id="service4"></span>
              </p>

              <hr>
            </div>
                <input class="btn btn-danger btn-block" type="submit" value="submit">
                </form>
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          
          <!-- /.box -->
        </div>
        <!-- /.col -->
 
        <!-- /.col -->
        <!-- /.col -->
       <div class="col-md-9">
          <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Gallery</a></li>
              <li><a href="#timeline" data-toggle="tab">Posts</a></li>
              <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
              
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
               <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="../userfiles/avatars/default.jpg" alt="user image">
                        <span class="username">
                          <a href="#"><?php echo $row['cname']; ?></a>   
                        </span>
                  </div>
                </div>
               <?php
	
	$stmt = $DB_con->prepare('SELECT picID,logos FROM ulnapics ORDER BY picID DESC');
	$stmt->execute();
	
	if($stmt->rowCount() > 0)
	{
		while($row=$stmt->fetch(PDO::FETCH_ASSOC))
		{
			extract($row);
			?>
                  
                
                <div class="post">
                  <div class="row margin-bottom">
                    <div class="col-sm-6">

                       <a class="btn-sm btn-success " href="images/addnew.php"><span class="glyphicon glyphicon-plus"></span> </a> 
                        <a class="btn-sm btn-info " href="images/editform.php?edit_id=<?php echo $row['picID']; ?>" title="click for edit" onclick="return confirm('You are about to edit the image ?')"><span class="glyphicon glyphicon-edit"></span> </a> 
                        <a class="btn-sm btn-danger " href="?delete_id=<?php echo $row['picID']; ?>" title="click for delete" onclick="return confirm('You are about to delete the image?')"><span class="glyphicon glyphicon-remove-circle"></span></a>
                       <img src="images/logos/<?php echo $row['logos']; ?>" class="img-responsive" width="250px" height="250px" />
                        

                <?php
		}
	}
	else
	{
      
   ?>
        <div class="col-xs-12">
        	<div class="alert alert-warning">
            	<span class="glyphicon glyphicon-info-sign"></span> &nbsp; You have not uploaded any images
            </div>
        </div>                                                      				 
        <?php
		
        
                   
        
	}
	
?>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-6">
                              <a class="btn-sm btn-success " href="images/addnew.php"><span class="glyphicon glyphicon-plus"></span> </a> 
                        <a class="btn-sm btn-info " href="images/editform.php?edit_id=<?php echo $row['logos']; ?>" title="click for edit" onclick="return confirm('You are about to edit the image ?')"><span class="glyphicon glyphicon-edit"></span> </a> 
                        <a class="btn-sm btn-danger " href="?delete_id=<?php echo $row['picID']; ?>" title="click for delete" onclick="return confirm('You are about to delete the image?')"><span class="glyphicon glyphicon-remove-circle"></span></a>
                          <img class="img-responsive" src="../userfiles/placeholder/placeholder1.png" alt="Photo">
                          <hr>
                              <a class="btn-sm btn-success " href="images/addnew.php"><span class="glyphicon glyphicon-plus"></span> </a> 
                        <a class="btn-sm btn-info " href="images/editform.php?edit_id=<?php echo $row['picID']; ?>" title="click for edit" onclick="return confirm('You are about to edit the image ?')"><span class="glyphicon glyphicon-edit"></span> </a> 
                        <a class="btn-sm btn-danger " href="?delete_id=<?php echo $row['picID']; ?>" title="click for delete" onclick="return confirm('You are about to delete the image?')"><span class="glyphicon glyphicon-remove-circle"></span></a>
                          <img class="img-responsive" src="../userfiles/placeholder/placeholder1.png" alt="Photo">
                             <hr>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                              <a class="btn-sm btn-success " href="images/addnew.php"><span class="glyphicon glyphicon-plus"></span> </a> 
                        <a class="btn-sm btn-info " href="images/editform.php?edit_id=<?php echo $row['picID']; ?>" title="click for edit" onclick="return confirm('You are about to edit the image ?')"><span class="glyphicon glyphicon-edit"></span> </a> 
                        <a class="btn-sm btn-danger " href="?delete_id=<?php echo $row['picID']; ?>" title="click for delete" onclick="return confirm('You are about to delete the image?')"><span class="glyphicon glyphicon-remove-circle"></span></a>
                          <img class="img-responsive" src="../userfiles/placeholder/placeholder1.png" alt="Photo">
                          <hr>
                              <a class="btn-sm btn-success " href="images/addnew.php"><span class="glyphicon glyphicon-plus"></span> </a> 
                        <a class="btn-sm btn-info " href="images/editform.php?edit_id=<?php echo $row['picID']; ?>" title="click for edit" onclick="return confirm('You are about to edit the image ?')"><span class="glyphicon glyphicon-edit"></span> </a> 
                        <a class="btn-sm btn-danger " href="?delete_id=<?php echo $row['picID']; ?>" title="click for delete" onclick="return confirm('You are about to delete the image?')"><span class="glyphicon glyphicon-remove-circle"></span></a>
                          <img class="img-responsive" src="../userfiles/placeholder/placeholder1.png" alt="Photo">
                             <hr>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->
                  </div>
    
                </div>
                <!-- /.post -->
              </div>
              <!-- /.timeline -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">Read more</a>
                        <a class="btn btn-danger btn-xs">Delete</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-user bg-aqua"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                      <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                      </h3>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-camera bg-purple"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                      <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                      <div class="timeline-body">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal">
                     <?php
        if(isset($_GET['error']))
		{
			?>
            <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Failed to save Details.Check your Internet Connection or Contact Support for Assistance</strong> 
			</div>
            <?php
		}
		?> 
                
                 <?php
        if(isset($_GET['success']))
		{
			?>
            <div class='alert alert-success'>
				<button class='close' data-dismiss='alert'>&times;</button>
				<strong>Details saved successfully.</strong> 
			</div>
            <?php
		}
		?> 
                  
                  
                 <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="company_email" name="company_email"placeholder="Email">
                    </div>
                  </div>
                <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Phone</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="company_phone" name="company_phone" placeholder="Phone">
                    </div>
                  </div>
                    <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Website</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="company_website" name="company_website" placeholder="Website">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" value="submit" class="btn btn-danger">Submit</button>
                    </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.7
    </div>
    <strong>Copyright &copy; 2015-2017 <a href="http://iteslink.com">ULNA</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i href="logout.php" class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
    </div>
<!-- ./wrapper -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- jQuery 2.2.3 -->
  <script src="../plugins/morris/morris.js"></script>  
  <script src="../plugins/morris/morris.min.js"></script> 

<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jQueryUI/jquery-ui.js"></script>
<script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../plugins/jQueryUI/jquery-ui.js"></script>
<script src="../plugins/jQueryUI/jquery-ui.min.js"></script>
    
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
    
   
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="../plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>
<script src="../dist/js/licence.js"></script>
    <script src="../dist/js/credit.js"></script>

   
    
</body>
</html>
