<?php

require_once 'dbconfig.php';

class SUBS
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function lasdID()
	{
		$stmt = $this->conn->lastInsertId();
		return $stmt;
	}
	
	public function subscription($cname,$code)
	{
		try
		{							
			$cname = md5($cname);
			$stmt = $this->conn->prepare("INSERT INTO subs(tbl_users.userName,cname,crep,subtype,token) 
			                                             VALUES(:tbl_users.userName, :cname, :crep,:subtype,:active_code)");
			$stmt->bindparam(":tbl_users.userName",$uname);
			$stmt->bindparam(":cname",$cname);
			$stmt->bindparam(":crep",$crep;
            $stmt->bindparam(":subtype",$subtype;
			$stmt->bindparam(":active_code",$code);
			$stmt->execute();	
			return $stmt;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
    
 
	
	public function subscribed($cname,$code)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT * FROM subs WHERE cname=:sub_id");
			$stmt->execute(array(":sub_id"=>$cname));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			
			if($stmt->rowCount() == 1)
			{
				if($userRow['subStatus']=="Y")
				{
					if($userRow['token']==md5($code))
					{
						$_SESSION['userSession'] = $userRow['sub_id'];
						return true;
					}
					else
					{
						header("Location: index.php?error");
						exit;
					}
				}
				else
				{
					header("Location: index.php?inactive");
					exit;
				}	
			}
			else
			{
				header("Location: index.php?error");
				exit;
			}		
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	
	public function is_subscribed()
	{
		if(isset($_SESSION['userSession']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function logout()
	{
		session_destroy();
		$_SESSION['userSession'] = false;
	}
	
	function send_mail($email,$message,$subject)
	{						
		require_once('mailer/class.phpmailer.php');
		$mail = new PHPMailer();
		$mail->IsSMTP(); 
		$mail->SMTPDebug  = 0;                     
		$mail->SMTPAuth   = true;                  
		$mail->SMTPSecure = "ssl";                 
		$mail->Host       = "smtp.gmail.com";      
		$mail->Port       = 465;             
		$mail->AddAddress($email);
		$mail->Username="intellectenquiries@gmail.com";  
		$mail->Password="Intellect2016$#";            
		$mail->SetFrom('intellectenquiries@gmail.com','ULNA');
		$mail->AddReplyTo("intellectenquiries@gmail.com","ULNA");
        $mail->Subject=($subject);
		$mail->MsgHTML($message);
		$mail->Send();
	}
    	
}